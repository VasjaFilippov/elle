﻿using GPSS.Parser.Language.SyntaxTrees;

namespace GPSS.Parser.Language
{
    public class SyntaxState
    {
        public Globals Globals { get; }

        public SyntaxState()
        {
            Globals = new Globals();
        }
    }
}