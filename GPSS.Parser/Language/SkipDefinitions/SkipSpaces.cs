﻿using Elle.Core.Data.Contexts;
using Elle.Core.Data.Intrefaces;

namespace GPSS.Parser.Language.SkipDefinitions
{
    public class SkipSpaces : ISkipDefinition
    {
        public bool TrySkip(LexerContext context)
        {
            var c = context.Current;
            if (c != '\n')
            {
                if (c != ' ' && c != '\t' && c != '\r')
                    return false;

                context.Next();
                return true;
            }
            
            context.Next();
            context.NextLine();
            return true;
        }
    }
}