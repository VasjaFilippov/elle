﻿using System.Collections.Generic;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.Scopes;
using Elle.Core.Data.TokenDefinitions;
using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.Scopes;
using GPSS.Parser.Language.SkipDefinitions;
using GPSS.Parser.Language.SyntaxTreeDefinitions;
using GPSS.Parser.Language.TokenDefinitions;
using GPSS.Parser.Language.Translators.Jass;

namespace GPSS.Parser.Language
{
    public class JassLanguage : ILanguage
    {
        public IEnumerable<ISkipDefinition> SkipDefinitions => new ISkipDefinition[]
        {
            new SkipComments(),
            new SkipSpaces(),
        };

        public IEnumerable<ITokenDefinition> Tokens => new ITokenDefinition[]
        {
            new WordDefinition(),
            new NumberDefinition(),
            new FloatDefinition(),
            new CharNumberDefinition(),
            new StringDefinition(),
            new OperatorDefinition(),
        };

        public IEnumerable<ISyntaxTreeDefinition> SyntaxTrees => new ISyntaxTreeDefinition[]
        {
            new FunctionDefinition(),
            new GlobalsDefinition(),
            new TypeDefinition(),
        };

        public IDictionary<int, ISyntaxTranslator> Translators => new Dictionary<int, ISyntaxTranslator>
        {
            {(int)SyntaxTags.FunctionDeclaration, new FunctionTranslator()},
            {(int)SyntaxTags.Globals, new GlobalsTranslator()},
            {(int)SyntaxTags.Type, new TypeTranslator()},
        };

        public IEnumerable<Word> ReservedWords => new[]
        {
            new Word((int)TokenTags.Native, "native"),
            new Word((int)TokenTags.Function, "function"),
            new Word((int)TokenTags.Takes, "takes"),
            new Word((int)TokenTags.Returns, "returns"),
            new Word((int)TokenTags.Endfunction, "endfunction"),
            new Word((int)TokenTags.Nothing, "nothing"),

            new Word((int)TokenTags.Globals, "globals"),
            new Word((int)TokenTags.Endglobals, "endglobals"),

            new Word((int)TokenTags.Array, "array"),
            new Word((int)TokenTags.Constant, "constant"),

            new Word((int)TokenTags.Type, "type"),
            new Word((int)TokenTags.Extends, "extends"),

            new Word((int)TokenTags.Local, "local"),
            new Word((int)TokenTags.Set, "set"),
            new Word((int)TokenTags.Call, "call"),
            new Word((int)TokenTags.Return, "return"),

            new Word((int)TokenTags.If, "if"),
            new Word((int)TokenTags.Then, "then"),
            new Word((int)TokenTags.Else, "else"),
            new Word((int)TokenTags.Elseif, "elseif"),
            new Word((int)TokenTags.Endif, "endif"),

            new Word((int)TokenTags.Loop, "loop"),
            new Word((int)TokenTags.Endloop, "endloop"),
            new Word((int)TokenTags.Exitwhen, "exitwhen"),

            new Word((int)TokenTags.And, "and"),
            new Word((int)TokenTags.Or, "or"),
            new Word((int)TokenTags.Not, "not"),
        };

        public IEnumerable<Declaration> DefaultDeclarations => new Declaration[]
        {
            new TypeInfo("integer"),
            new TypeInfo("real"),
            new TypeInfo("boolean"),
            new TypeInfo("string"),
            new TypeInfo("handle"),
            new TypeInfo("code"),
        };

        public IEnumerable<IPreprocessorCommand> PreprocessorCommands => new IPreprocessorCommand[]
        {
            
        };

        public IEnumerable<IPreprocessorDetector> PreprocessorDetectors => new IPreprocessorDetector[]
        {
            
        };
    }
}