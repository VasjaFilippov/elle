﻿using System.Collections.Generic;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.Scopes;
using Elle.Core.Data.TokenDefinitions;
using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.Preprocessor;

namespace GPSS.Parser.Language
{
    public class GpssCore : ILanguage
    {
        public IEnumerable<ISkipDefinition> SkipDefinitions => new ISkipDefinition[]
        {

        };

        public IEnumerable<ITokenDefinition> Tokens => new ITokenDefinition[]
        {

        };

        public IEnumerable<ISyntaxTreeDefinition> SyntaxTrees => new ISyntaxTreeDefinition[]
        {

        };

        public IDictionary<int, ISyntaxTranslator> Translators => new Dictionary<int, ISyntaxTranslator>
        {

        };

        public IEnumerable<Word> ReservedWords => new Word[]
        {
            new Word((int) TokenTags.Include, "include"),
            new Word((int) TokenTags.Class, "class"),
            new Word((int) TokenTags.Model, "model"),
            new Word((int) TokenTags.Field, "field"),
            new Word((int) TokenTags.Property, "property"),
            new Word((int) TokenTags.Get, "get"),
            new Word((int) TokenTags.This, "this"),
            new Word((int) TokenTags.Constructor, "constructor"),
        };

        public IEnumerable<Declaration> DefaultDeclarations => new Declaration[]
        {

        };

        public IEnumerable<IPreprocessorCommand> PreprocessorCommands => new IPreprocessorCommand[]
        {
            new IncludePreprocessor(),
        };
        
        public IEnumerable<IPreprocessorDetector> PreprocessorDetectors => new IPreprocessorDetector[]
        {
            new PreprocessorDetector(),
        };
    }
}