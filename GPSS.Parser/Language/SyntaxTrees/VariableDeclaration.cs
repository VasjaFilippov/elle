﻿using System.Diagnostics;
using Elle.Core.Data.SyntaxTrees;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.Scopes;
using GPSS.Parser.Language.SyntaxTrees.Expressions;

namespace GPSS.Parser.Language.SyntaxTrees
{
    [DebuggerDisplay("{DebugView}")]
    public class VariableDeclaration : SyntaxTree
    {
        public VariableInfo Info { get; set; }
        public Expression Value { get; set; }

        public string Name => Info.Name;
        public TypeInfo Type => Info.Type;
        public bool IsArray => Info.IsArray;
        public bool IsConstant => Info.IsConstant;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string DebugViewArray => Info?.IsArray ?? false ? "array " : "";

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string DebugViewConstant => Info?.IsConstant ?? false ? "constant " : "";
        
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string DebugView => $"{DebugViewConstant}{Info?.Type?.Name ?? "unknown"} {DebugViewArray}{Info?.Name ?? "unknown"}";

        public override int Tag => (int)SyntaxTags.VariableDeclaration;

        public VariableDeclaration()
        {
            
        }

        public VariableDeclaration(VariableInfo info)
        {
            Info = info;
        }
    }
}