﻿using Elle.Core.Data.SyntaxTrees;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.SyntaxTrees.Expressions;

namespace GPSS.Parser.Language.SyntaxTrees.Statements
{
    public class ExitwhenStatement : SyntaxTree
    {
        public override int Tag => (int) SyntaxTags.Exitwhen;

        public Expression Value { get; set; }
    }
}