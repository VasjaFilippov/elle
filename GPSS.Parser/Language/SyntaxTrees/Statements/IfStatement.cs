﻿using System.Collections.Generic;
using Elle.Core.Data.SyntaxTrees;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.SyntaxTrees.Expressions;

namespace GPSS.Parser.Language.SyntaxTrees.Statements
{
    public class IfStatement : SyntaxTree
    {
        public class Body
        {
            public Expression Condition { get; set; }
            public List<SyntaxTree> Statements { get; } = new List<SyntaxTree>();
        }

        public override int Tag => (int) SyntaxTags.If;
        public Body Then { get; } = new Body();
        public List<Body> ElseIfs { get; } = new List<Body>();
        public List<SyntaxTree> Else { get; } = new List<SyntaxTree>();
    }
}