﻿using System.Collections.Generic;
using Elle.Core.Data.SyntaxTrees;
using GPSS.Parser.Language.Enums;

namespace GPSS.Parser.Language.SyntaxTrees.Statements
{
    public class LoopStatement : SyntaxTree
    {
        public override int Tag => (int) SyntaxTags.Loop;

        public List<SyntaxTree> Body { get; } = new List<SyntaxTree>();
    }
}