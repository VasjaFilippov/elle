﻿using Elle.Core.Data.SyntaxTrees;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.SyntaxTrees.Expressions;

namespace GPSS.Parser.Language.SyntaxTrees.Statements
{
    public class SetStatement : SyntaxTree
    {
        public override int Tag => (int) SyntaxTags.Set;

        public Operand Variable { get; set; }
        public Expression Value { get; set; }
    }
}