﻿using Elle.Core.Data.SyntaxTrees;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.Scopes;

namespace GPSS.Parser.Language.SyntaxTrees.Expressions
{
    public abstract class Expression : SyntaxTree
    {
        public override int Tag => (int) SyntaxTags.Expression;
        public TypeInfo ReturnType { get; set; }
    }
}