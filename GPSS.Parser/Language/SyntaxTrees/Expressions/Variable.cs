﻿namespace GPSS.Parser.Language.SyntaxTrees.Expressions
{
    public class IdentifierOperand : LeftHandOperand
    {
        public string Name { get; set; }
    }
}