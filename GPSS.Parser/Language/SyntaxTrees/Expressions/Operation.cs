﻿using Elle.Core.Data.Tokens;

namespace GPSS.Parser.Language.SyntaxTrees.Expressions
{
    public class Operation : Expression
    {
        public Token Operator { get; set; }
        public Expression LeftOperand { get; set; }
        public Expression RightOperand { get; set; }
    }
}