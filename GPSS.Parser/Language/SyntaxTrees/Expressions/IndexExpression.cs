﻿using System.Collections.Generic;

namespace GPSS.Parser.Language.SyntaxTrees.Expressions
{
    public class IndexExpression : LeftHandOperand
    {
        public string Name { get; set; }
        public List<Expression> Indices { get; } = new List<Expression>();
    }
}