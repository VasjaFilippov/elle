﻿namespace GPSS.Parser.Language.SyntaxTrees.Expressions
{
    public class ExpressionInBrackets : Expression
    {
         public Expression Inner { get; set; }
    }
}