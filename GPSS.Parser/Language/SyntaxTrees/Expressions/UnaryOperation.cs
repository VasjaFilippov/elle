﻿using Elle.Core.Data.Tokens;

namespace GPSS.Parser.Language.SyntaxTrees.Expressions
{
    public class UnaryOperation : Expression
    {
        public Token Operator { get; set; } 
        public Expression Inner { get; set; }
    }
}