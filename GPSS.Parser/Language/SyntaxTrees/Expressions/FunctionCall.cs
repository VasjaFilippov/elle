﻿using System.Collections.Generic;

namespace GPSS.Parser.Language.SyntaxTrees.Expressions
{
    public class FunctionCall : Expression
    {
        public string Name { get; set; }
        public List<Expression> Parameters { get; } = new List<Expression>();
    }
}