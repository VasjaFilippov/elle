﻿using Elle.Core.Data.Tokens;

namespace GPSS.Parser.Language.SyntaxTrees.Expressions
{
    public class ValueOperand : Operand
    {
        public Token Value { get; set; }
    }
}