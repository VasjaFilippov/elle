﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Elle.Core.Data.SyntaxTrees;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.Scopes;

namespace GPSS.Parser.Language.SyntaxTrees
{
    [DebuggerDisplay("{DebugView}")]
    public class Function : SyntaxTree
    {
        public FunctionInfo Info { get; set; }
        public bool IsConstant => Info.IsConstant;
        public bool IsNative => Info.IsNative;
        public string Name => Info.Name;
        public TypeInfo ReturnType => Info.ReturnType;
        public List<SyntaxTree> Body { get; } = new List<SyntaxTree>();
        public List<VariableInfo> Parameters => Info.Parameters;
        
        public string TypeParameterList => "nothing";

        public string ParameterList
            => Parameters.Any()
                ? string.Join(", ", Parameters.Select(p => $"{p.Type.Name} {p.Name}"))
                : "nothing";

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string DebugViewConstant
            => IsConstant
                ? "constant "
                : "";

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string DebugViewNative
            => IsNative
                ? "native "
                : "";

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string ReturnTypeOrNothing
            => ReturnType?.Name ?? "nothing";

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string DebugView => $"{DebugViewNative}{DebugViewConstant}{ReturnTypeOrNothing} {Name}<{TypeParameterList}>({ParameterList})";

        public override int Tag => (int) SyntaxTags.FunctionDeclaration;

        public Function()
        {
            
        }

        public Function(FunctionInfo info)
        {
            Info = info;
        }
    }
}