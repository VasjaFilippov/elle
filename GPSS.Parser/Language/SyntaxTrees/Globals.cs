﻿using System.Collections.Generic;
using Elle.Core.Data.SyntaxTrees;
using GPSS.Parser.Language.Enums;

namespace GPSS.Parser.Language.SyntaxTrees
{
    public class Globals : SyntaxTree
    {
        public override int Tag => (int) SyntaxTags.Globals;
        public List<VariableDeclaration> Variables { get; } = new List<VariableDeclaration>();
    }
}