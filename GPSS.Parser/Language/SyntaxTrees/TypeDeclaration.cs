﻿using System.Collections.Generic;
using Elle.Core.Data.SyntaxTrees;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.Scopes;

namespace GPSS.Parser.Language.SyntaxTrees
{
    public class TypeDeclaration : SyntaxTree
    {
        public TypeInfo Info { get; set; }
        public override int Tag => (int) SyntaxTags.Type;

        public string Name => Info.Name;
        public List<TypeInfo> BaseTypes => Info.BaseTypes;

        public TypeDeclaration()
        {
            
        }

        public TypeDeclaration(TypeInfo info)
        {
            Info = info;
        }
    }
}