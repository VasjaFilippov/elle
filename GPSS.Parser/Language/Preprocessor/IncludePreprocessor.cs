﻿using Elle.Core.Data.Contexts;
using Elle.Core.Data.Intrefaces;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.Tokens;

namespace GPSS.Parser.Language.Preprocessor
{
    public class IncludePreprocessor : IPreprocessorCommand
    {
        public bool CanBeStart(PreprocessorContext context)
            => context.Current.Tag == (int) TokenTags.Include;

        public void Preprocess(PreprocessorContext context)
        {
            context.Next();

            var path = (StringToken) context.ValidateSymbol("Path expected", (int) TokenTags.String);
            context.InsertSource(path.Value);
        }
    }
}