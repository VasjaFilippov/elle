﻿using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.Tokens;

namespace GPSS.Parser.Language.Preprocessor
{
    public class PreprocessorDetector : IPreprocessorDetector
    {
        public bool IsPreprocessorStart(Token token)
            => token.Tag == '#';
    }
}