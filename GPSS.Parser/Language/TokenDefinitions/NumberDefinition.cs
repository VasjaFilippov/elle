﻿using Elle.Core.Data.Contexts;
using Elle.Core.Data.TokenDefinitions;
using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Tokens;

namespace GPSS.Parser.Language.TokenDefinitions
{
    public class NumberDefinition : ITokenDefinition
    {
        private static readonly FloatDefinition FloatDefinition = new FloatDefinition();

        public bool CanBeStart(LexerContext context) => char.IsDigit(context.Current);

        public Token TryParse(LexerContext context)
        {
            var v = 0;

            do
            {
                v = v * 10 + (context.Current - '0');

                if (context.Next() == '.')
                    return FloatDefinition.TryParse(v, context);
            }
            while (char.IsDigit(context.Current));

            return new Number(v);
        }
    }
}