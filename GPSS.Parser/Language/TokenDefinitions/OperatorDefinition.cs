﻿using System;
using Elle.Core.Data.Contexts;
using Elle.Core.Data.TokenDefinitions;
using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.Tokens;

namespace GPSS.Parser.Language.TokenDefinitions
{
    public class OperatorDefinition : ITokenDefinition
    {
        public bool CanBeStart(LexerContext context)
        {
            var tag = context.Current;
            return tag == '+' || tag == '-' || tag == '*' || tag == '/' || tag == '<' || tag == '>' || tag == '=' || tag == '!';
        }

        public Token TryParse(LexerContext context)
        {
            switch (context.Pop())
            {
                case '+':
                    return new OperatorToken((int) TokenTags.Add);
                case '-':
                    return new OperatorToken((int) TokenTags.Substract);
                case '*':
                    return new OperatorToken((int) TokenTags.Multiply);
                case '/':
                    return new OperatorToken((int) TokenTags.Divide);
                case '!':
                    if (context.Pop() != '=')
                        throw new Exception("Unknown operator");
                    return new OperatorToken((int) TokenTags.NotEquals);
                case '=':
                    if (context.Current != '=')
                        return new OperatorToken((int) TokenTags.SetOperator);
                    context.Next();
                    return new OperatorToken((int) TokenTags.EqualsOperator);
                case '>':
                    if (context.Current != '=')
                        return new OperatorToken((int) TokenTags.More);
                    context.Next();
                    return new OperatorToken((int) TokenTags.NotLess);
                case '<':
                    if (context.Current != '=')
                        return new OperatorToken((int) TokenTags.Less);
                    context.Next();
                    return new OperatorToken((int) TokenTags.NotMore);
                default:
                    throw new Exception("Unknown opeartor");
            }
        }
    }
}