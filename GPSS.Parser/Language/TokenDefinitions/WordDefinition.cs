﻿using System.Text;
using Elle.Core.Data.Contexts;
using Elle.Core.Data.Enums;
using Elle.Core.Data.TokenDefinitions;
using Elle.Core.Data.Tokens;

namespace GPSS.Parser.Language.TokenDefinitions
{
    public class WordDefinition : ITokenDefinition
    {
        public bool CanBeStart(LexerContext context) => char.IsLetter(context.Current);

        public Token TryParse(LexerContext context)
        {
            var sb = new StringBuilder();

            do
            {
                sb.Append(context.Pop());
            } while (char.IsLetterOrDigit(context.Current) || context.Current == '_');

            var s = sb.ToString();
            Word w;

            if (context.Words.TryGetValue(s, out w))
                return w;

            w = new Word((int)Tags.Id, s);
            context.ReserveWord(w);
            return w;
        }
    }
}