﻿using Elle.Core.Data.Contexts;
using Elle.Core.Data.TokenDefinitions;
using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Tokens;

namespace GPSS.Parser.Language.TokenDefinitions
{
    public class FloatDefinition : ITokenDefinition
    {
        public bool CanBeStart(LexerContext context) => context.Current == '.';

        public Token TryParse(LexerContext context) => TryParse(0, context);

        public Token TryParse(int start, LexerContext context)
        {
            var result = (float)start;
            var exp = 0.1f;
            
            while (char.IsDigit(context.Next()))
            {
                result = result + (context.Current - '0') * exp;
                exp *= 0.1f;
            }

            return new Float(result);
        }
    }
}