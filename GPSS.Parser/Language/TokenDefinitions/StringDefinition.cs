using Elle.Core.Data.Contexts;
using Elle.Core.Data.TokenDefinitions;
using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Tokens;

namespace GPSS.Parser.Language.TokenDefinitions
{
    public class StringDefinition : ITokenDefinition
    {
        public bool CanBeStart(LexerContext context)
            => context.Current == '\"';

        public Token TryParse(LexerContext context)
        {
            var charNum = "";

            while (context.Next() != '\"')
                charNum += context.Current;

            context.Next();
            return new StringToken(charNum);
        }
    }
}