﻿using System.Collections.Generic;
using Elle.Core.Data.Scopes;

namespace GPSS.Parser.Language.Scopes
{
    public class TypeInfo : Declaration
    {
        public List<TypeInfo> BaseTypes { get; } = new List<TypeInfo>();

        public TypeInfo()
        {
            
        }

        public TypeInfo(string name)
        {
            Name = name;
            ScopeName = $"type {name}";
        }
    }
}