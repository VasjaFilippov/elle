﻿using System;
using System.Collections.Generic;
using Elle.Core.Data.Contexts;
using Elle.Core.Data.Enums;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.SyntaxTrees;
using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.Scopes;
using GPSS.Parser.Language.SyntaxTrees.Expressions;

namespace GPSS.Parser.Language.SyntaxTreeDefinitions.Expressions
{
    public class ExpressionDefinition : ISyntaxTreeDefinition
    {
        private readonly FunctionCallDefinition _functionCallDefinition;
        private readonly IndexDefinition _indexDefinition;

        private static readonly Dictionary<TokenTags, int> OperationPriorities = new Dictionary<TokenTags, int>
        {
            {TokenTags.And, 25},
            {TokenTags.Or, 25},
            {TokenTags.More, 50},
            {TokenTags.Less, 50},
            {TokenTags.NotLess, 50},
            {TokenTags.NotMore, 50},
            {TokenTags.EqualsOperator, 50},
            {TokenTags.NotEquals, 50},
            {TokenTags.Add, 100},
            {TokenTags.Substract, 100},
            {TokenTags.Multiply, 150},
            {TokenTags.Divide, 150},
        };

        public ExpressionDefinition()
        {
            _functionCallDefinition = new FunctionCallDefinition(this);
            _indexDefinition = new IndexDefinition(this);
        }

        public bool CanBeStart(SyntaxContext context)
        {
            var tagId = context.Current.Tag;
            var tag = (Tags) tagId;
            var tokenTag = (TokenTags) tagId;
            return tag == Tags.Id
                || tokenTag == TokenTags.Float || tokenTag == TokenTags.Num || tokenTag == TokenTags.CharNumber || tokenTag == TokenTags.String
                || tokenTag == TokenTags.True || tokenTag == TokenTags.False
                || tagId == '('
                || tokenTag == TokenTags.Add || tokenTag == TokenTags.Substract || tokenTag == TokenTags.Not || tokenTag == TokenTags.Function;
        }

        private static bool CanBeOperator(Token token)
            => OperationPriorities.ContainsKey((TokenTags) token.Tag);

        public Expression Parse(SyntaxContext context)
        {
            if (!CanBeStart(context))
                throw new Exception("Expression expected");
            return (Expression)TryParse(context);
        }

        private ExpressionInBrackets ParseBrackets(SyntaxContext context)
        {
            var expr = Parse(context);
            context.ValidateSymbol("Closing bracket expected", ')');
            return new ExpressionInBrackets
            {
                Inner = expr,
                ReturnType = expr.ReturnType,
            };
        }

        private Expression ParseUnaryInner(SyntaxContext context)
        {
            var left = context.Pop();

            switch (left.Tag)
            {
                case '(':
                    return ParseBrackets(context);
                case (int) TokenTags.Not:
                case (int) TokenTags.Substract:
                case (int) TokenTags.Add:
                case (int) TokenTags.Function:
                    return ParseUnary(context, left);
                case (int) Tags.Id:
                    switch (context.Current.Tag)
                    {
                        case '(':
                            return ParseFunctionCall(context, left);
                        case '[':
                            return ParseIndex(context, left);
                        default:
                            return new IdentifierOperand { Name = (Word)left, };
                    }
                default:
                    var operand = new ValueOperand { Value = left, };
                    switch ((TokenTags) left.Tag)
                    {
                        case TokenTags.Num:
                        case TokenTags.CharNumber:
                            operand.ReturnType = context.CurrentScope.FindDeclaration<TypeInfo>("integer", "type");
                            break;
                        case TokenTags.String:
                            operand.ReturnType = context.CurrentScope.FindDeclaration<TypeInfo>("string", "type");
                            break;
                        case TokenTags.Float:
                            operand.ReturnType = context.CurrentScope.FindDeclaration<TypeInfo>("real", "type");
                            break;
                        case TokenTags.True:
                        case TokenTags.False:
                            operand.ReturnType = context.CurrentScope.FindDeclaration<TypeInfo>("boolean", "type");
                            break;
                    }
                    return operand;
            }
        }

        private UnaryOperation ParseUnary(SyntaxContext context, Token left)
        {
            var inner = ParseUnaryInner(context);
            var operation = new UnaryOperation
            {
                Operator = left,
                Inner = inner,
                ReturnType = inner.ReturnType,
            };

            return operation;
        }

        private Operation ParseOperation(SyntaxContext context, Expression left)
        {
            var operation = new Operation
            {
                LeftOperand = left,
                Operator = context.Pop(),
                RightOperand = Parse(context),
                ReturnType = left.ReturnType,
            };

            if (!OperationPriorities.ContainsKey((TokenTags) operation.Operator.Tag))
                throw new Exception("Unknown operator");

            var rightOperation = operation.RightOperand as Operation;
            if (rightOperation == null || OperationPriorities[(TokenTags) operation.Operator.Tag] <= OperationPriorities[(TokenTags) rightOperation.Operator.Tag])
                return operation;

            operation = new Operation
            {
                Operator = rightOperation.Operator,
                LeftOperand = new Operation
                {
                    Operator = operation.Operator,
                    LeftOperand = operation.LeftOperand,
                    RightOperand = rightOperation.LeftOperand,
                    ReturnType = left.ReturnType,
                },
                RightOperand = rightOperation.RightOperand,
                ReturnType = left.ReturnType,
            };

            return operation;
        }

        private FunctionCall ParseFunctionCall(SyntaxContext context, Token left)
        {
            context.Push(left);
            var function = (FunctionCall) _functionCallDefinition.TryParse(context);
            return function;
        }

        private IndexExpression ParseIndex(SyntaxContext context, Token left)
        {
            context.Push(left);
            var index = (IndexExpression) _indexDefinition.TryParse(context);
            return index;
        }

        public SyntaxTree TryParse(SyntaxContext context)
        {
            var leftOperand = ParseUnaryInner(context);

            return context.Current != null && CanBeOperator(context.Current)
                ? ParseOperation(context, leftOperand)
                : leftOperand;
        }
    }
}