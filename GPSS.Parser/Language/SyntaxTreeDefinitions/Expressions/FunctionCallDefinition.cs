﻿using Elle.Core.Data.Contexts;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.SyntaxTrees;
using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Scopes;
using GPSS.Parser.Language.SyntaxTrees.Expressions;

namespace GPSS.Parser.Language.SyntaxTreeDefinitions.Expressions
{
    public class FunctionCallDefinition : ISyntaxTreeDefinition
    {
        private readonly ExpressionDefinition _expressionDefinition;

        public FunctionCallDefinition(ExpressionDefinition expressionDefinition)
        {
            _expressionDefinition = expressionDefinition;
        }

        public bool CanBeStart(SyntaxContext context)
        {
            throw new System.NotImplementedException();
        }

        public SyntaxTree TryParse(SyntaxContext context)
        {
            var function = new FunctionCall { Name = (Word) context.Pop(), };

            function.ReturnType = context.CurrentScope.FindDeclaration<FunctionInfo>(function.Name, "function").ReturnType;
            context.ValidateSymbol("Opening bracket expected", '(');

            if (context.Current.Tag != ')')
                function.Parameters.Add((Expression)_expressionDefinition.TryParse(context));

            while (context.Current.Tag != ')')
            {
                context.ValidateSymbol("Parameter separator expected", ',');
                function.Parameters.Add((Expression)_expressionDefinition.TryParse(context));
            }

            context.ValidateSymbol("Closing bracket expected", ')');
            return function;
        }
    }
}