﻿using Elle.Core.Data.Contexts;
using Elle.Core.Data.Enums;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.SyntaxTrees;
using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Scopes;
using GPSS.Parser.Language.SyntaxTrees.Expressions;

namespace GPSS.Parser.Language.SyntaxTreeDefinitions.Expressions
{
    public class IndexDefinition : ISyntaxTreeDefinition
    {
        private readonly ExpressionDefinition _expressionDefinition;

        public IndexDefinition(ExpressionDefinition expressionDefinition)
        {
            _expressionDefinition = expressionDefinition;
        }

        public bool CanBeStart(SyntaxContext context)
            => context.Current.Tag == (int) Tags.Id;

        public SyntaxTree TryParse(SyntaxContext context)
        {
            var expr = new IndexExpression { Name = (Word)context.Pop(), };
            expr.ReturnType = context.CurrentScope.FindDeclaration<VariableInfo>(expr.Name, "variable").Type;

            context.ValidateSymbol("Opening bracket expected", '[');
            expr.Indices.Add(_expressionDefinition.Parse(context));

            while (context.Current.Tag != ']')
            {
                context.ValidateSymbol("Index separator expected", ',');
                expr.Indices.Add(_expressionDefinition.Parse(context));
            }

            context.ValidateSymbol("Closing bracket expected", ']');
            return expr;
        }
    }
}