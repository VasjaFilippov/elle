﻿using System;
using Elle.Core.Data.Contexts;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.SyntaxTrees;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.SyntaxTrees;

namespace GPSS.Parser.Language.SyntaxTreeDefinitions
{
    public class GlobalsDefinition : ISyntaxTreeDefinition
    {
        private static readonly VariableDefinition VariableDefinition = new VariableDefinition();
        private bool _globalsSaved;

        public bool CanBeStart(SyntaxContext context)
            => context.Current.Tag == (int) TokenTags.Globals;

        public SyntaxTree TryParse(SyntaxContext context)
        {
            if (context.State == null)
                context.State = new SyntaxState();

            var globals = ((SyntaxState)context.State).Globals;
            context.GlobalScope(() =>
            {
                context.Next();
                while (context.Current.Tag != (int) TokenTags.Endglobals)
                {
                    if (!VariableDefinition.CanBeStart(context))
                        throw new Exception("Only variables allowed in globals section");

                    var variable = (VariableDeclaration) VariableDefinition.TryParse(context);
                    globals.Variables.Add(variable);
                }
            });
            context.ValidateSymbol("Endglobals missing", (int)TokenTags.Endglobals);

            context.SkipParsedTree = true;
            if (_globalsSaved)
                return globals;

            context.RestTrees.Add(globals);
            _globalsSaved = true;

            return globals;
        }
    }
}