﻿using System;
using Elle.Core.Data.Contexts;
using Elle.Core.Data.Enums;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.SyntaxTrees;
using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.Scopes;
using GPSS.Parser.Language.SyntaxTreeDefinitions.Expressions;
using GPSS.Parser.Language.SyntaxTrees;

namespace GPSS.Parser.Language.SyntaxTreeDefinitions
{
    public class VariableDefinition : ISyntaxTreeDefinition
    {
        private static readonly ExpressionDefinition ExprParser = new ExpressionDefinition();

        public bool CanBeStart(SyntaxContext context)
            => context.Current.Tag == (int) Tags.Id || context.Current.Tag == (int) TokenTags.Constant;

        public VariableDeclaration Parse(SyntaxContext context, bool arrayAllowed, bool constAllowed)
        {
            var isConstant = false;
            var isArray = false;
            Word name;

            if (context.Current.Tag == (int) TokenTags.Constant)
            {
                if (!constAllowed)
                    throw new Exception("Constant not allowed here");
                isConstant = true;
                context.Next();
            }

            var type = ((Word) context.Pop()).Value;
            var token = context.Pop();

            switch (token.Tag)
            {
                case (int)Tags.Id:
                    name = (Word) token;
                    break;
                case (int)TokenTags.Array:
                    if (!arrayAllowed)
                        throw new Exception("Array not allowed here");
                    if (isConstant)
                        throw new Exception("Constant can not be array");
                    isArray = true;
                    name = (Word) context.ValidateSymbol("Variable name expected", (int)Tags.Id);
                    break;
                default:
                    throw new Exception("Variable name expected");
            }

            var info = context.CurrentScope.Register<VariableInfo>($"variable {name.Value}", true);
            info.Name = name.Value;
            info.IsDeclared = true;
            info.IsArray = isArray;
            info.IsConstant = isConstant;
            info.Type = context.CurrentScope.FindDeclaration<TypeInfo>(type, "type");
            var variable = new VariableDeclaration(info);

            if (context.Current.Tag != (int) TokenTags.SetOperator)
            {
                if (variable.IsConstant)
                    throw new Exception("Constant must be initialized");
                return variable;
            }

            if (variable.IsArray)
                throw new Exception("Array variables can not be initialized");
            
            context.Next();
            variable.Value = ExprParser.Parse(context);

            return variable;
        }

        public SyntaxTree TryParse(SyntaxContext context)
            => Parse(context, true, true);
    }
}