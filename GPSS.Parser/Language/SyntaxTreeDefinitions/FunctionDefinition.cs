﻿using System;
using System.Collections.Generic;
using Elle.Core.Data.Contexts;
using Elle.Core.Data.Enums;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.Scopes;
using Elle.Core.Data.SyntaxTrees;
using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.Scopes;
using GPSS.Parser.Language.SyntaxTreeDefinitions.Expressions;
using GPSS.Parser.Language.SyntaxTrees;
using GPSS.Parser.Language.SyntaxTrees.Expressions;
using GPSS.Parser.Language.SyntaxTrees.Statements;

namespace GPSS.Parser.Language.SyntaxTreeDefinitions
{
    public class FunctionDefinition : ISyntaxTreeDefinition
    {
        private class FuncMetaData
        {
            public string Name;
            public bool IsNative;
            public bool IsConstant;
            public string ReturnType;
            public readonly List<VariableInfo> Parameters = new List<VariableInfo>();

            public string ScopeName => $"function {Name}";
        }

        private static readonly VariableDefinition VariableDefinition = new VariableDefinition();
        private static readonly ExpressionDefinition ExpressionDefinition = new ExpressionDefinition();
        private static readonly FunctionCallDefinition FunctionCallDefinition = new FunctionCallDefinition(ExpressionDefinition);
        private static readonly IndexDefinition IndexDefinition = new IndexDefinition(ExpressionDefinition);

        public bool CanBeStart(SyntaxContext context)
            => context.Current.Tag == (int)TokenTags.Function
            || context.Current.Tag == (int)TokenTags.Constant
            || context.Current.Tag == (int)TokenTags.Native;

        private void ParseParameter(Scope scope, FuncMetaData func, Word typeName, string name)
        {
            var paramInfo = scope.Register<VariableInfo>($"variable {name}", true);
            paramInfo.Name = name;
            paramInfo.IsConstant = false;
            paramInfo.IsDeclared = true;
            paramInfo.Type = scope.FindDeclaration<TypeInfo>(typeName, "type");
            
            func.Parameters.Add(paramInfo);
        }

        private FuncMetaData ParseSignature(SyntaxContext context)
        {
            var func = new FuncMetaData();

            if (context.Current.Tag == (int)TokenTags.Constant)
            {
                func.IsConstant = true;
                context.Next();
            }

            if (context.Current.Tag == (int)TokenTags.Native)
            {
                func.IsNative = true;
                context.Next();
            }
            else
                context.ValidateSymbol("function keyword expected", (int)TokenTags.Function);

            func.Name = (Word) context.ValidateSymbol("Function name expected", (int)Tags.Id);
            context.ValidateSymbol("Parameter list expected", (int)TokenTags.Takes);
            var token = context.ValidateSymbol("Parameter list expected", (int)TokenTags.Nothing, (int)Tags.Id);

            if (token.Tag != (int) TokenTags.Nothing)
            {
                context.Push(token);
                do
                {
                    ParseParameter(context.CurrentScope, func,
                        (Word) context.ValidateSymbol("Parameter type expected", (int) Tags.Id),
                        (Word) context.ValidateSymbol("Parameter name expected", (int) Tags.Id)
                    );

                    token = context.ValidateSymbol("Parameter separator expected", ',', (int) TokenTags.Returns);
                } while (token.Tag != (int) TokenTags.Returns);
            }
            else
            {
                context.ValidateSymbol("Return type expected", (int)TokenTags.Returns);
            }

            var retType = (Word) context.ValidateSymbol("Return type expected", (int)TokenTags.Nothing, (int)Tags.Id);
            func.ReturnType = retType.Tag == (int) TokenTags.Nothing
                ? null
                : retType;

            return func;
        }

        private VariableDeclaration ParseLocal(SyntaxContext context, FunctionInfo funcDecl)
        {
            context.Next();
            var def = VariableDefinition.Parse(context, true, true);

            return def;
        }

        private SetStatement ParseSet(SyntaxContext context)
        {
            context.Next();
            var vname = (Word)context.ValidateSymbol("Identifier expected", (int)Tags.Id);
            Operand leftPart;
            if (context.Current.Tag == '[')
            {
                context.Push(vname);
                leftPart = (IndexExpression)IndexDefinition.TryParse(context);
            }
            else
            {
                leftPart = new IdentifierOperand { Name = vname, };
            }
            context.ValidateSymbol("Operator = expected", (int) TokenTags.SetOperator);
            var value = ExpressionDefinition.Parse(context);
            return new SetStatement
            {
                Variable = leftPart,
                Value = value,
            };
        }

        private SyntaxTree ParseCall(SyntaxContext context)
        {
            context.Next();
            return FunctionCallDefinition.TryParse(context);
        }

        private ReturnStatement ParseReturn(SyntaxContext context, Function func)
        {
            context.Next();
            if (ExpressionDefinition.CanBeStart(context))
            {
                if (func.ReturnType == null)
                    throw new Exception("Return is forbidden in functions returning nothing");
                return new ReturnStatement { Value = ExpressionDefinition.Parse(context), };
            }

            if (func.ReturnType != null)
                throw new Exception("Return nothing is forbidden in functions returning something");
            return new ReturnStatement();
        }

        private void ReadBody(SyntaxContext context, ICollection<SyntaxTree> body, Function func, FunctionInfo funcDecl, bool allowExitwhen)
        {
            while (context.Current.Tag != (int)TokenTags.Endif
                && context.Current.Tag != (int)TokenTags.Elseif
                && context.Current.Tag != (int)TokenTags.Else)
                body.Add(ParseStatement(context, func, funcDecl, allowExitwhen));
        }

        private List<SyntaxTree> PrepareBody(SyntaxContext context, IfStatement ifst)
        {
            switch ((TokenTags)context.Current.Tag)
            {
                case TokenTags.Elseif:
                    context.Next();
                    var eib = new IfStatement.Body { Condition = ExpressionDefinition.Parse(context), };
                    ifst.ElseIfs.Add(eib);
                    context.ValidateSymbol("then keyword expected", (int)TokenTags.Then);
                    return eib.Statements;
                case TokenTags.Else:
                    context.Next();
                    return ifst.Else;
                default:
                    return null;
            }
        }

        private IfStatement ParseIf(SyntaxContext context, Function func, FunctionInfo funcDecl, bool allowExitwhen)
        {
            var ifst = new IfStatement();
            context.Next();
            ifst.Then.Condition = ExpressionDefinition.Parse(context);
            context.ValidateSymbol("then keyword expected", (int) TokenTags.Then);

            var body = ifst.Then.Statements;
            ReadBody(context, body, func, funcDecl, allowExitwhen);
            body = PrepareBody(context, ifst);

            while (body != null)
            {
                ReadBody(context, body, func, funcDecl, allowExitwhen);
                body = PrepareBody(context, ifst);
            }

            context.ValidateSymbol("endif keyword expected", (int) TokenTags.Endif);
            return ifst;
        }

        private SyntaxTree ParseLoop(SyntaxContext context, Function func, FunctionInfo funcDecl)
        {
            context.Next();
            var loop = new LoopStatement();
            while (context.Current.Tag != (int) TokenTags.Endloop)
                loop.Body.Add(ParseStatement(context, func, funcDecl, true));
            context.ValidateSymbol("endloop keyword expected", (int)TokenTags.Endloop);
            return loop;
        }
        
        private SyntaxTree ParseExitwhen(SyntaxContext context, bool allowExitwhen)
        {
            if (!allowExitwhen)
                throw new Exception("Exitwhen outside of loop not allowed");
            context.Next();

            var ews = new ExitwhenStatement
            {
                Value = ExpressionDefinition.Parse(context)
            };
            return ews;
        }

        private SyntaxTree ParseStatement(SyntaxContext context, Function func, FunctionInfo funcDecl, bool allowExitwhen)
        {
            switch ((TokenTags) context.Current.Tag)
            {
                case TokenTags.Local:       return ParseLocal(context, funcDecl);
                case TokenTags.Set:         return ParseSet(context);
                case TokenTags.Call:        return ParseCall(context);
                case TokenTags.Return:      return ParseReturn(context, func);
                case TokenTags.If:          return ParseIf(context, func, funcDecl, allowExitwhen);
                case TokenTags.Loop:        return ParseLoop(context, func, funcDecl);
                case TokenTags.Exitwhen:    return ParseExitwhen(context, allowExitwhen);
                default:                    throw new Exception("Unknown expression in function body");
            }
        }
        
        public SyntaxTree TryParse(SyntaxContext context)
        {
            Function func = null;

            context.Scope(() =>
            {
                var fmd = ParseSignature(context);
                var funcScopeName = fmd.ScopeName;
                var funcDecl = context.CurrentScope.Parent.Register<FunctionInfo>(funcScopeName, true);
                funcDecl.Name = fmd.Name;
                funcDecl.IsDeclared = true;
                funcDecl.IsConstant = fmd.IsConstant;
                funcDecl.IsNative = fmd.IsNative;
                funcDecl.Parameters.AddRange(fmd.Parameters);
                if (fmd.ReturnType != null)
                    funcDecl.ReturnType = context.CurrentScope.FindDeclaration<TypeInfo>(fmd.ReturnType, "type");
                func = new Function(funcDecl);

                if (!funcDecl.IsNative)
                {
                    while (context.Current != null && context.Current.Tag != (int)TokenTags.Endfunction)
                        func.Body.Add(ParseStatement(context, func, funcDecl, false));

                    context.ValidateSymbol("endfunction keyword expected", (int) TokenTags.Endfunction);
                }

                return funcScopeName;
            });


            return func;
        }
    }
}