﻿using Elle.Core.Data.Contexts;
using Elle.Core.Data.Enums;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.SyntaxTrees;
using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.Scopes;
using GPSS.Parser.Language.SyntaxTrees;

namespace GPSS.Parser.Language.SyntaxTreeDefinitions
{
    public class TypeDefinition : ISyntaxTreeDefinition
    {
        public bool CanBeStart(SyntaxContext context)
            => context.Current.Tag == (int) TokenTags.Type;

        public SyntaxTree TryParse(SyntaxContext context)
        {   
            context.Next();
            var name = ((Word) context.ValidateSymbol("Type name expected", (int) Tags.Id)).Value;
            context.ValidateSymbol("extends keyword expected", (int) TokenTags.Extends);
            var baseType = (Word) context.ValidateSymbol("Base type expected", (int) Tags.Id);

            var info = context.CurrentScope.Register<TypeInfo>($"type {name}", true);
            info.Name = name;
            info.IsDeclared = true;
            info.BaseTypes.Add(context.CurrentScope.FindDeclaration<TypeInfo>(baseType, "type"));

            var type = new TypeDeclaration(info);
            return type;
        }
    }
}