﻿using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Enums;

namespace GPSS.Parser.Language.Tokens
{
    public class CharNumber : Token<string>
    {
        public CharNumber(string value) : base((int) TokenTags.CharNumber, value)
        {
        }
    }
}