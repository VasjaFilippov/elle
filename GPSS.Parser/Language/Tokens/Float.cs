﻿using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Enums;

namespace GPSS.Parser.Language.Tokens
{
    public class Float : Token<float>
    {
        public Float(float value) : base((int)TokenTags.Float, value)
        {
            
        }
    }
}
