﻿using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Enums;

namespace GPSS.Parser.Language.Tokens
{
    public class Number : Token<int>
    {
        public Number(int value) : base((int)TokenTags.Num, value)
        {
        }
    }
}