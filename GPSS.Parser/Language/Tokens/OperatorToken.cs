﻿using Elle.Core.Data.Tokens;

namespace GPSS.Parser.Language.Tokens
{
    public class OperatorToken : Token
    {
        public OperatorToken(int tag) : base(tag)
        {
        }
    }
}