﻿using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Enums;

namespace GPSS.Parser.Language.Tokens
{
    public class StringToken : Token<string>
    {
        public StringToken(string value) : base((int) TokenTags.String, value)
        {
        }
    }
}