﻿using System;
using System.Linq;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.SyntaxTrees;
using Elle.Core.Data.Tokens;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.SyntaxTrees.Expressions;
using GPSS.Parser.Language.Tokens;

namespace GPSS.Parser.Language.Translators.Jass
{
    public class ExpressionTranslator : ISyntaxTranslator
    {
        private string TranslateOperator(Token token)
        {
            switch ((TokenTags) token.Tag)
            {
                case TokenTags.Add:
                    return "+";
                case TokenTags.Substract:
                    return "-";
                case TokenTags.Multiply:
                    return "*";
                case TokenTags.Divide:
                    return "/";
                case TokenTags.More:
                    return ">";
                case TokenTags.NotLess:
                    return ">=";
                case TokenTags.Less:
                    return "<";
                case TokenTags.NotMore:
                    return "<=";
                case TokenTags.EqualsOperator:
                    return "==";
                case TokenTags.NotEquals:
                    return "!=";
                case TokenTags.And:
                    return "and";
                case TokenTags.Or:
                    return "or";
                case TokenTags.Not:
                    return "not";
                case TokenTags.Function:
                    return "function";
                default:
                    throw new Exception("Unknown operator");
            }
        }

        public string Translate(SyntaxTree tree)
        {
            var fCall = tree as FunctionCall;
            if (fCall != null)
                return $"{fCall.Name}({string.Join(",", fCall.Parameters.Select(Translate))})";

            var index = tree as IndexExpression;
            if (index != null)
                return $"{index.Name}[{string.Join(",", index.Indices.Select(Translate))}]";

            var brackets = tree as ExpressionInBrackets;
            if (brackets != null)
                return $"({Translate(brackets.Inner)})";

            var unary = tree as UnaryOperation;
            if (unary != null)
                return $"{TranslateOperator(unary.Operator)}{Translate(unary.Inner)}";

            var operation = tree as Operation;
            if (operation != null)
                return $"{Translate(operation.LeftOperand)}{TranslateOperator(operation.Operator)}{Translate(operation.RightOperand)}";

            var identifier = tree as IdentifierOperand;
            if (identifier != null)
                return identifier.Name;

            var operand = tree as ValueOperand;
            if (operand != null)
            {
                var value = operand.Value;
                switch ((TokenTags) value.Tag)
                {
                    case TokenTags.Float:
                        return ((Float)value).Value.ToString();
                    case TokenTags.Num:
                        return ((Number)value).Value.ToString();
                    case TokenTags.CharNumber:
                        return $"'{((CharNumber)value).Value}'";
                    case TokenTags.String:
                        return $"\"{((StringToken)value).Value}\"";
                }
            }

            throw new Exception("Unknown expression");
        }
    }
}