﻿using System.Text;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.SyntaxTrees;
using GPSS.Parser.Language.SyntaxTrees;

namespace GPSS.Parser.Language.Translators.Jass
{
    public class VariableDeclarationTranslator : ISyntaxTranslator
    {
        private readonly ExpressionTranslator _exprTranslator;

        public VariableDeclarationTranslator(ExpressionTranslator exprTranslator)
        {
            _exprTranslator = exprTranslator;
        }

        public string Translate(VariableDeclaration variable)
        {
            var varStr = new StringBuilder();

            if (variable.IsConstant)
                varStr.Append("constant ");
            varStr.Append($"{variable.Type.Name}");

            if (variable.IsArray)
                varStr.Append(" array");
            varStr.Append($" {variable.Name}");

            if (variable.Value != null)
                varStr.Append($"={_exprTranslator.Translate(variable.Value)}");

            return varStr.ToString();
        }

        public string Translate(SyntaxTree tree)
            => Translate((VariableDeclaration) tree);
    }
}