﻿using System.Text;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.SyntaxTrees;
using GPSS.Parser.Language.SyntaxTrees;

namespace GPSS.Parser.Language.Translators.Jass
{
    public class GlobalsTranslator : ISyntaxTranslator
    {
        private static readonly ExpressionTranslator ExpressionTranslator = new ExpressionTranslator();
        private static readonly VariableDeclarationTranslator VariableTranslator = new VariableDeclarationTranslator(ExpressionTranslator);

        public string Translate(SyntaxTree tree)
        {
            var globals = (Globals) tree;

            var globalsText = new StringBuilder("globals\r\n");

            foreach (var variable in globals.Variables)
                globalsText.AppendLine($"    {VariableTranslator.Translate(variable)}");

            globalsText.Append("endglobals");
            return globalsText.ToString();
        }
    }
}