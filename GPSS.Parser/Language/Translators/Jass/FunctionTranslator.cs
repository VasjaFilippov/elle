﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.SyntaxTrees;
using GPSS.Parser.Language.Enums;
using GPSS.Parser.Language.SyntaxTrees;
using GPSS.Parser.Language.SyntaxTrees.Statements;

namespace GPSS.Parser.Language.Translators.Jass
{
    public class FunctionTranslator : ISyntaxTranslator
    {
        private static readonly ExpressionTranslator ExpressionTranslator = new ExpressionTranslator();
        private static readonly VariableDeclarationTranslator VariableTranslator = new VariableDeclarationTranslator(ExpressionTranslator);

        private void TranslateIf(StringBuilder strFunc, IfStatement ifst, string intendation)
        {
            strFunc.AppendLine($"if {ExpressionTranslator.Translate(ifst.Then.Condition)} then");
            TranslateBody(strFunc, ifst.Then.Statements, $"{intendation}    ");

            foreach (var body in ifst.ElseIfs)
            {
                strFunc.AppendLine($"{intendation}elseif {ExpressionTranslator.Translate(body.Condition)} then");
                TranslateBody(strFunc, body.Statements, $"{intendation}    ");
            }
            
            if (ifst.Else.Any())
            {
                strFunc.AppendLine($"{intendation}else");
                TranslateBody(strFunc, ifst.Else, $"{intendation}    ");
            }

            strFunc.AppendLine($"{intendation}endif");
        }

        private void TranslateLoop(StringBuilder strFunc, LoopStatement loop, string intendation)
        {
            strFunc.AppendLine("loop");
            TranslateBody(strFunc, loop.Body, $"{intendation}    ");
            strFunc.AppendLine("endloop");
        }

        private void TranslateBody(StringBuilder strFunc, IEnumerable<SyntaxTree> body, string intendation = "    ")
        {
            foreach (var statement in body)
            {
                strFunc.Append(intendation);
                switch ((SyntaxTags)statement.Tag)
                {
                    case SyntaxTags.VariableDeclaration:
                        strFunc.AppendLine($"local {VariableTranslator.Translate(statement)}");
                        break;
                    case SyntaxTags.Set:
                        var set = (SetStatement)statement;
                        strFunc.AppendLine($"set {ExpressionTranslator.Translate(set.Variable)}={ExpressionTranslator.Translate(set.Value)}");
                        break;
                    case SyntaxTags.Return:
                        var ret = (ReturnStatement) statement;
                        strFunc.AppendLine(ret.Value != null
                            ? $"return {ExpressionTranslator.Translate(ret.Value)}"
                            : "return");
                        break;
                    case SyntaxTags.Expression:
                        strFunc.AppendLine($"call {ExpressionTranslator.Translate(statement)}");
                        break;
                    case SyntaxTags.If:
                        TranslateIf(strFunc, (IfStatement)statement, intendation);
                        break;
                    case SyntaxTags.Loop:
                        TranslateLoop(strFunc, (LoopStatement)statement, intendation);
                        break;
                    case SyntaxTags.Exitwhen:
                        strFunc.AppendLine($"exitwhen {ExpressionTranslator.Translate(((ExitwhenStatement)statement).Value)}");
                        break;
                }
            }
        }

        public string Translate(SyntaxTree tree)
        {
            var func = (Function) tree;
            var strFunc = new StringBuilder();

            if (func.IsConstant)
                strFunc.Append("constant ");

            if (func.IsNative)
                strFunc.Append("native ");

            strFunc.Append($"function {func.Name} takes {func.ParameterList} returns {func.ReturnType?.Name ?? "nothing"}");

            if (func.IsNative)
                return strFunc.ToString();

            strFunc.AppendLine();
            TranslateBody(strFunc, func.Body);
            strFunc.Append("endfunction");

            return strFunc.ToString();
        }
    }
}