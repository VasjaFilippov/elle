﻿using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.SyntaxTrees;
using GPSS.Parser.Language.SyntaxTrees;

namespace GPSS.Parser.Language.Translators.Jass
{
    public class TypeTranslator : ISyntaxTranslator
    {
        public string Translate(SyntaxTree tree)
        {
            var type = (TypeDeclaration) tree;
            return $"type {type.Name} extends {type.BaseTypes[0].Name}";
        }
    }
}