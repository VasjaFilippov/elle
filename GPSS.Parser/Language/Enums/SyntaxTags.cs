﻿namespace GPSS.Parser.Language.Enums
{
    public enum SyntaxTags
    {
        FunctionDeclaration,
        VariableDeclaration,
        Globals,
        Expression,
        Type,
        Set,
        Return,
        If,
        Exitwhen,
        Loop
    }
}