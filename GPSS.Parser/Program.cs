﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Elle.Core.Data.Scopes;
using Elle.Core.Data.Tokens;
using Elle.Core.Logics;
using Elle.Core.Utilities;
using GPSS.Parser.Language;
using GPSS.Parser.Language.Scopes;
using GPSS.Parser.Language.Tokens;

namespace GPSS.Parser
{
    internal static class Program
    {
        private static bool TryWriteToken<TToken, TValue>(this TextWriter writer, Token token, string format = "{0}")
            where TToken : Token<TValue>
        {
            var num = token as TToken;
            if (num == null) return false;
            writer.Write(format, num.Value);
            return true;
        }

        private static void PrintToken(Token token, TextWriter tokensFile)
        {
            tokensFile.Write($"{token.Tag}\t\t");

            if (tokensFile.TryWriteToken<Word, string>(token) ||
                tokensFile.TryWriteToken<Number, int>(token) ||
                tokensFile.TryWriteToken<Float, float>(token) ||
                tokensFile.TryWriteToken<CharNumber, string>(token, "'{0}'") ||
                tokensFile.TryWriteToken<StringToken, string>(token, "\"{0}\""))
                return;
            tokensFile.Write((char)token.Tag);
        }

        private static void PrintDeclaration(Declaration declaration, TextWriter declsFile)
        {
            if (declaration.IsDeclared)
                declsFile.Write("declared ------ ");
            else
                declsFile.Write("not declared -- ");

            declsFile.Write($"{declaration.FullPath}");

            if (declaration is VariableInfo)
            {
                var variable = (VariableInfo)declaration;
                if (variable.IsConstant)
                    declsFile.Write(" constant");
                if (variable.IsArray)
                    declsFile.Write(" array");
                declsFile.Write($" of type {variable.Type.ScopeName}");
                declsFile.WriteLine();
                return;
            }

            if (declaration is FunctionInfo)
            {
                var func = (FunctionInfo)declaration;
                if (func.IsConstant)
                    declsFile.Write(" constant");
                if (func.IsNative)
                    declsFile.Write(" native");
                if (func.ReturnType != null)
                    declsFile.Write($" returning {func.ReturnType.ScopeName}");
                declsFile.WriteLine();
                return;
            }

            if (declaration is TypeInfo)
            {
                var type = (TypeInfo)declaration;
                if (type.BaseTypes.Any())
                    declsFile.Write($":\r\n\t\t{string.Join("\r\n\t\t", type.BaseTypes.Select(t => t.FullPath))}");
                else if (type.BaseTypes.Any())
                    declsFile.Write($": {type.BaseTypes[0].FullPath}");
                declsFile.WriteLine();
                return;
            }
        }

        private static void Main()
        {
            // Language
            var gpssCore = new GpssCore();
            var jass = new JassLanguage();
            var gpss = new AssembledLanguage(jass, gpssCore);
            
            // Translation chain
            var translator = new TranslationChain();
            var codeGen = new CodeGenerator();

#if PRINT_DEBUG
            var prevCount = 0;
            var tCount = 0;
            Console.SetCursorPosition(0, 1);
            Console.Write("Tokens: 0");

            translator.SetTokenCatcher(token =>
            {
                if (++tCount - prevCount < 100) return;
                prevCount = tCount;
                Console.SetCursorPosition(8, 1);
                Console.Write(tCount);
            });
#endif

            if (!Directory.Exists("Out"))
                Directory.CreateDirectory("Out");

            var timer = new Stopwatch();
            timer.Start();

            translator.ParseDependancies(gpss, @"Library\Libs.j");

            using (var tokensFile = File.CreateText(@"Out\Tokens.etf"))
            {
                translator.SetTokenCatcher(token =>
                {
                    PrintToken(token, tokensFile);
                    tokensFile.WriteLine();
#if PRINT_DEBUG
                    if (++tCount - prevCount < 100) return;
                    Console.SetCursorPosition(8, 1);
                    Console.Write(++tCount);
#endif
                });

                translator.Prepare(gpss, @"In\TestCode.j");
                codeGen.Prepare(jass, translator);

                using (var outFile = File.CreateText(@"Out\Code.j"))
                    while (codeGen.HasNext())
                    {
                        outFile.WriteLine(codeGen.Next());
                        outFile.WriteLine();
                    }
                
                using (var declsFile = File.CreateText(@"Out\Declarations.edf"))
                    foreach (var declaration in translator.Declarations.OrderBy(d => d.IsDeclared))
                        PrintDeclaration(declaration, declsFile);
            }

            timer.Stop();
#if PRINT_DEBUG
            Console.SetCursorPosition(8, 1);
            Console.Write(tCount);
#endif
            Console.SetCursorPosition(0, 2);
            Console.WriteLine($"Finished in {timer.ElapsedMilliseconds} ms");
            Console.ReadKey();
        }
    }
}
