﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.SyntaxTrees;
using Elle.Core.Exceptions;
using Elle.Test.Cil.Enums;
using Elle.Test.Cil.Scopes;
using Elle.Test.Cil.SyntaxTrees;
using Elle.Test.Cil.SyntaxTrees.Operators;

namespace Elle.Test.Hsail.Translators
{
    public class FunctionTranslator : ISyntaxTranslator
    {
        private static string TranslateOperator(Operator op)
        {
            switch ((SyntaxTags) op.Tag)
            {
                case SyntaxTags.Call:
                    switch (((CallOperator) op).Name)
                    {
                        case "Hsail.Api::GetGlobalId":
                            return "workitemabsid_u32\t";
                        default:
                            throw new NotImplementedException();
                    }
                case SyntaxTags.Clt:
                    return "cmp_lt_b1_u32\t\t";
                case SyntaxTags.Ret:
                    return "ret\t\t\t\t\t";
                case SyntaxTags.Add:
                    return "add_f32\t\t\t\t";
                case SyntaxTags.Sub:
                    return "sub_f32\t\t\t\t";
                case SyntaxTags.Brtrue:
                    return "cbr_b1\t\t\t\t";
                default:
                    throw new NotImplementedException();
            }
        }

        private static string TranslateArg(Operator op, SyntaxTree operation)
        {
            switch ((SyntaxTags)op.Tag)
            {
                case SyntaxTags.Ldc:
                    return $"{((NumOperator)op).Value}";
                case SyntaxTags.Ldloc:
                    char reg;
                    switch ((SyntaxTags) (operation?.Tag ?? 0))
                    {
                        case SyntaxTags.Brtrue:
                            reg = 'c';
                            break;
                        default:
                            reg = 's';
                            break;
                    }
                    return $"${reg}{((NumOperator)op).Value}";
                case SyntaxTags.Ldarg:
                    return $"[%arg_val{((NumOperator)op).Value}]";
                default:
                    throw new NotImplementedException();
            }
        }

        private static string TranslateTarget(Operator op, SyntaxTree operation)
        {
            switch ((SyntaxTags) op.Tag)
            {
                case SyntaxTags.Stloc:
                    char reg;
                    switch ((SyntaxTags) (operation?.Tag ?? 0))
                    {
                        case SyntaxTags.Clt:
                            reg = 'c';
                            break;
                        default:
                            reg = 's';
                            break;
                    }
                    return $"${reg}{((NumOperator) op).Value}";
                default:
                    throw new NotImplementedException();
            }
        }

        private static void TranslateOperation(StringBuilder strFunc, Operator op, Operator target, IReadOnlyList<Operator> args)
        {
            if (op == null)
            {
                if (args.Count > 1)
                    throw new ParseException("Operator is missing");

                var arg = args[0];
                if (arg.Tag != (int) SyntaxTags.Ldarg || target.Tag != (int) SyntaxTags.Stloc)
                    throw new NotImplementedException();

                if (arg.Label.Usages > 0)
                    strFunc
                        .Append(arg.Label.Name)
                        .Append(": ");
                else
                    strFunc.Append("\t\t");

                strFunc
                    .Append("ld_kernarg_u32\t\t")
                    .Append(TranslateTarget(target, op))
                    .Append(", ")
                    .Append(TranslateArg(arg, op))
                    .AppendLine(";");

                return;
            }
            
            if (args.Any() && args[0].Label.Usages > 0)
                strFunc
                    .Append(args[0].Label.Name)
                    .Append(":\t");
            else if (op.Label.Usages > 0)
                strFunc
                    .Append(op.Label.Name)
                    .Append(":\t");
            else
                strFunc.Append("\t\t");

            // TODO: придумать, как сдвигать индекс массива
            /*.Append("shl_u32 ")
            .Append(TranslateTarget(target, op))
            .Append(", ")
            .Append(TranslateArg(args[1], op))
            .AppendLine(", 2")*/

            switch ((SyntaxTags) op.Tag)
            {
                case SyntaxTags.Ldelem:
                    strFunc.Append("add_u32\t\t\t\t")
                        .Append(TranslateTarget(target, op))
                        .Append(", ")
                        .Append(TranslateArg(args[0], op))
                        .Append(", ")
                        .Append(TranslateArg(args[1], op))
                        .AppendLine(";")

                        .Append("\t\tld_global_f32\t\t")
                        .Append(TranslateTarget(target, op))
                        .Append(", [")
                        .Append(TranslateTarget(target, op))
                        .AppendLine("];");

                    return;
                case SyntaxTags.Stelem:
                    strFunc
                        .Append("add_u32\t\t\t\t")
                        .Append(TranslateArg(args[1], op))
                        .Append(", ")
                        .Append(TranslateArg(args[1], op))
                        .Append(", ")
                        .Append(TranslateArg(args[2], op))
                        .AppendLine(";")

                        .Append("\t\tst_global_f32\t\t")
                        .Append(TranslateArg(args[0], op))
                        .Append(", [")
                        .Append(TranslateArg(args[1], op))
                        .AppendLine("];");
                    return;
            }

            strFunc.Append(TranslateOperator(op));
            var firstArg = target == null;

            if (!firstArg)
                strFunc.Append(TranslateTarget(target, op));

            foreach (var arg in args)
            {
                if (!firstArg)
                    strFunc.Append(", ");

                strFunc.Append(TranslateArg(arg, op));
                firstArg = false;
            }

            switch ((SyntaxTags) op.Tag)
            {
                case SyntaxTags.Brtrue:
                case SyntaxTags.Brfalse:
                    if (!firstArg)
                        strFunc.Append(", ");

                    strFunc.Append(((LabelOperator) op).Value.Name);
                    firstArg = false;
                    break;
            }

            strFunc.AppendLine(";");
        }

        private static void TranslateBody(StringBuilder strFunc, IEnumerable<SyntaxTree> body)
        {
            if (!body.Any())
            {
                strFunc.AppendLine();
                return;
            }

            var args = new List<Operator>();
            Operator operation = null;

            foreach (Operator statement in body)
            {
                switch ((SyntaxTags) statement.Tag)
                {
                    case SyntaxTags.Nop:
                        continue;
                    case SyntaxTags.Ldc:
                    case SyntaxTags.Ldloc:
                    case SyntaxTags.Ldarg:
                        args.Add(statement);
                        break;
                    case SyntaxTags.Call:
                    case SyntaxTags.Clt:
                    case SyntaxTags.Add:
                    case SyntaxTags.Sub:
                    case SyntaxTags.Ret:
                    case SyntaxTags.Ldelem:
                        operation = statement;
                        break;
                    case SyntaxTags.Stloc:
                        TranslateOperation(strFunc, operation, statement, args);
                        operation = null;
                        args.Clear();
                        break;
                    case SyntaxTags.Stelem:
                        TranslateOperation(strFunc, statement, null, args);
                        operation = null;
                        args.Clear();
                        break;
                    case SyntaxTags.Brfalse:
                    case SyntaxTags.Brtrue:
                        TranslateOperation(strFunc, statement, null, args);
                        operation = null;
                        args.Clear();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            if (operation != null || args.Any())
            TranslateOperation(strFunc, operation, null, args);
        }

        private static string TranslateType(TypeInfo t)
        {
            switch (t.Name)
            {
                case "int32":
                    return "kernarg_u32";
                case "float32":
                    return "kernarg_u32";
                default:
                    throw new NotImplementedException();
            }
        }

        private static string TranslateParameters(Function f)
            => f.Parameters.Any()
                ? string.Join(",\r\n\t\t", f.Parameters.Select(p => $"{TranslateType(p.Type)} %arg_val{p.Name}"))
                : "";

        public string Translate(SyntaxTree tree)
        {
            var func = (Function) tree;
            var strFunc = new StringBuilder();

            var parameters = TranslateParameters(func);
            if (parameters.Length > 0)
                parameters = $"\r\n\t\t{parameters}";

            strFunc.AppendLine($"kernel &{func.Name}({parameters})");

            strFunc.AppendLine("{");
            TranslateBody(strFunc, func.Body);
            strFunc.AppendLine("}");

            return strFunc.ToString();
        }
    }
}