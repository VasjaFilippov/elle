﻿using System.Collections.Generic;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.Scopes;
using Elle.Core.Data.TokenDefinitions;
using Elle.Core.Data.Tokens;
using Elle.Test.Cil.Enums;
using Elle.Test.Hsail.Translators;

namespace Elle.Test.Hsail
{
    public class Language : ILanguage
    {
        public IEnumerable<ISkipDefinition> SkipDefinitions => new ISkipDefinition[]
        {

        };

        public IEnumerable<ITokenDefinition> Tokens => new ITokenDefinition[]
        {

        };

        public IEnumerable<ISyntaxTreeDefinition> SyntaxTrees => new ISyntaxTreeDefinition[]
        {

        };

        public IDictionary<int, ISyntaxTranslator> Translators => new Dictionary<int, ISyntaxTranslator>
        {
            {(int) SyntaxTags.Function, new FunctionTranslator()},
        };

        public IEnumerable<Word> ReservedWords => new Word[]
        {

        };

        public IEnumerable<Declaration> DefaultDeclarations => new Declaration[]
        {

        };

        public IEnumerable<IPreprocessorCommand> PreprocessorCommands => new IPreprocessorCommand[]
        {
            
        };

        public IEnumerable<IPreprocessorDetector> PreprocessorDetectors => new IPreprocessorDetector[]
        {
            
        };
    }
}