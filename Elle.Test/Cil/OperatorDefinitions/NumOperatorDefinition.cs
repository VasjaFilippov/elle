﻿using System.Collections.Generic;
using Elle.Core.Data.Contexts;
using Elle.Test.Cil.Enums;
using Elle.Test.Cil.SyntaxTrees;
using Elle.Test.Cil.SyntaxTrees.Operators;
using Elle.Test.Cil.Tokens;

namespace Elle.Test.Cil.OperatorDefinitions
{
    public class NumOperatorDefinition : IOperatorDefinition
    {
        public Operator TryParse(SyntaxContext context, string name, Dictionary<string, Label> labelTable)
        {
            var parameters = name.Split('.');

            if (parameters.Length < 2 || parameters[1] == "s")
                return new NumOperator(_tag, (Number) context.ValidateSymbol("Id expected", (int) Tags.Number));

            var id = parameters[1];
            return new NumOperator(_tag, int.Parse(id));
        }

        public NumOperatorDefinition(SyntaxTags tag)
        {
            _tag = (int) tag;
        }

        private readonly int _tag;
    }
}