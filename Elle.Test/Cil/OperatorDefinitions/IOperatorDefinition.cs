﻿using System.Collections.Generic;
using Elle.Core.Data.Contexts;
using Elle.Test.Cil.SyntaxTrees;
using Elle.Test.Cil.SyntaxTrees.Operators;

namespace Elle.Test.Cil.OperatorDefinitions
{
    public interface IOperatorDefinition
    {
        Operator TryParse(SyntaxContext context, string name, Dictionary<string, Label> labelTable);
    }
}