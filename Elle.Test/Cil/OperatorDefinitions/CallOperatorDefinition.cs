﻿using System.Collections.Generic;
using Elle.Core.Data.Contexts;
using Elle.Core.Data.Enums;
using Elle.Core.Data.Tokens;
using Elle.Test.Cil.SyntaxTrees;
using Elle.Test.Cil.SyntaxTrees.Operators;

namespace Elle.Test.Cil.OperatorDefinitions
{
    public class CallOperatorDefinition : IOperatorDefinition
    {
        public Operator TryParse(SyntaxContext context, string name, Dictionary<string, Label> labelTable)
        {
            var op = new CallOperator
            {
                ReturnType = (Word) context.ValidateSymbol("Return type expected", (int) Tags.Id),
                Name = ""
            };

            while (context.Current != null && context.Current?.Tag != '(')
            {
                if (context.Current.Tag == (int) Tags.Id)
                    op.Name += (Word) context.Current;
                else
                    op.Name += (char) context.Current.Tag;

                context.Next();
            }

            context.ValidateSymbol("Parameter type list expected", '(');
            var firstParam = true;
            while (context.Current != null && context.Current?.Tag != ')')
            {
                if (!firstParam)
                    context.ValidateSymbol("Parameter separator expected", ',');
                op.ParameterTypes.Add((Word)context.Pop());
                firstParam = false;
            }
            context.ValidateSymbol("Parameter type list end expected", ')');

            return op;
        }
    }
}