﻿using System;
using System.Collections.Generic;
using Elle.Core.Data.Contexts;
using Elle.Test.Cil.Enums;
using Elle.Test.Cil.SyntaxTrees;
using Elle.Test.Cil.SyntaxTrees.Operators;

namespace Elle.Test.Cil.OperatorDefinitions
{
    public class TrivialOperatorDefinition : IOperatorDefinition
    {
        public Operator TryParse(SyntaxContext context, string name, Dictionary<string, Label> labelTable) => new TrivialOperator(_tag);

        public TrivialOperatorDefinition(SyntaxTags tag)
        {
            _tag = (int) tag;
        }

        private readonly int _tag;
    }
}