﻿using System.Collections.Generic;
using Elle.Core.Data.Contexts;
using Elle.Core.Data.Tokens;
using Elle.Test.Cil.Enums;
using Elle.Test.Cil.SyntaxTrees;
using Elle.Test.Cil.SyntaxTrees.Operators;
using Elle.Test.Utilities;
using Tags = Elle.Core.Data.Enums.Tags;

namespace Elle.Test.Cil.OperatorDefinitions
{
    public class LabelOperatorDefinition : IOperatorDefinition
    {
        public Operator TryParse(SyntaxContext context, string name, Dictionary<string, Label> labelTable)
        {
            var lblName = (Word) context.ValidateSymbol("Label name expected", (int) Tags.Id);

            var lbl = labelTable.FindOrCreate(lblName);
            lbl.Usages++;

            return new LabelOperator(_tag, lbl);
        }

        public LabelOperatorDefinition(SyntaxTags tag)
        {
            _tag = (int) tag;
        }

        private readonly int _tag;
    }
}