﻿using System.Collections.Generic;
using Elle.Core.Data.Contexts;
using Elle.Test.Cil.Enums;
using Elle.Test.Cil.SyntaxTrees;
using Elle.Test.Cil.SyntaxTrees.Operators;

namespace Elle.Test.Cil.OperatorDefinitions
{
    public class LdcOperatorDefinition : IOperatorDefinition
    {
        public Operator TryParse(SyntaxContext context, string name, Dictionary<string, Label> labelTable)
        {
            var parameters = name.Split('.');
            var value = parameters[2];
            return new NumOperator((int) SyntaxTags.Ldc, int.Parse(value));
        }
    }
}