﻿namespace Elle.Test.Cil.Enums
{
    public enum Tags
    {
        Method = 257,
        Public,
        Hidebysig,
        Static,
        Void,
        Cil,
        Managed,
        Maxstack,
        Locals,
        Init,
        Int32,
        Float32,
        Bool,
        Nop,
        Ldc,
        Call,
        Stloc,
        Ldarg,
        Clt,
        Brfalse,
        Ldelem,
        Add,
        Stelem,
        Ret,
        Number
    }
}