﻿namespace Elle.Test.Cil.Enums
{
    public enum SyntaxTags
    {
        Function = 1,
        Nop,
        Ldc,
        Call,
        Stloc,
        Clt,
        Add,
        Ldloc,
        Ldarg,
        Ret,
        Brfalse,
        Brtrue,
        Ldelem,
        Stelem,
        Sub
    }
}