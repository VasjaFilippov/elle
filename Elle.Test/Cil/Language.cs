﻿using System.Collections.Generic;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.Scopes;
using Elle.Core.Data.TokenDefinitions;
using Elle.Core.Data.Tokens;
using Elle.Test.Cil.Enums;
using Elle.Test.Cil.OperatorDefinitions;
using Elle.Test.Cil.Scopes;
using Elle.Test.Cil.SkipDefinitions;
using Elle.Test.Cil.SyntaxTreeDefinitions;
using Elle.Test.Cil.SyntaxTrees.Operators;
using Elle.Test.Cil.TokenDefinitions;

namespace Elle.Test.Cil
{
    public class Language : ILanguage
    {
        public IEnumerable<ISkipDefinition> SkipDefinitions => new ISkipDefinition[]
        {
            new SkipSpaces(),
            new SkipComments(),
        };

        public IEnumerable<ITokenDefinition> Tokens => new ITokenDefinition[]
        {
            new WordDefinition(),
            new NumberDefinition(),
        };

        public IEnumerable<Word> ReservedWords => new[]
        {
            new Word((int)Tags.Method, ".method"),
            new Word((int)Tags.Public, "public"),
            new Word((int)Tags.Hidebysig, "hidebysig"),
            new Word((int)Tags.Static, "static"),
            new Word((int)Tags.Void, "void"),
            new Word((int)Tags.Cil, "cil"),
            new Word((int)Tags.Managed, "managed"),
            new Word((int)Tags.Maxstack, ".maxstack"),
            new Word((int)Tags.Locals, ".locals"),
            new Word((int)Tags.Init, "init"),
        };

        public IDictionary<int, ISyntaxTranslator> Translators => new Dictionary<int, ISyntaxTranslator>
        {
            
        };

        public IEnumerable<ISyntaxTreeDefinition> SyntaxTrees => new ISyntaxTreeDefinition[]
        {
            new FunctionDefinition
            {
                Operators =
                {
                    { "call", new CallOperatorDefinition() },
                    { "nop", new TrivialOperatorDefinition(SyntaxTags.Nop) },
                    { "clt", new TrivialOperatorDefinition(SyntaxTags.Clt) },
                    { "add", new TrivialOperatorDefinition(SyntaxTags.Add) },
                    { "sub", new TrivialOperatorDefinition(SyntaxTags.Sub) },
                    { "ret", new TrivialOperatorDefinition(SyntaxTags.Ret) },

                    { "ldelem.i1", new TrivialOperatorDefinition(SyntaxTags.Ldelem) },
                    { "ldelem.i2", new TrivialOperatorDefinition(SyntaxTags.Ldelem) },
                    { "ldelem.i4", new TrivialOperatorDefinition(SyntaxTags.Ldelem) },
                    { "ldelem.i8", new TrivialOperatorDefinition(SyntaxTags.Ldelem) },
                    { "ldelem.u1", new TrivialOperatorDefinition(SyntaxTags.Ldelem) },
                    { "ldelem.u2", new TrivialOperatorDefinition(SyntaxTags.Ldelem) },
                    { "ldelem.u4", new TrivialOperatorDefinition(SyntaxTags.Ldelem) },
                    { "ldelem.u8", new TrivialOperatorDefinition(SyntaxTags.Ldelem) },
                    { "ldelem.r4", new TrivialOperatorDefinition(SyntaxTags.Ldelem) },
                    { "ldelem.r8", new TrivialOperatorDefinition(SyntaxTags.Ldelem) },
                    { "ldelem.i", new TrivialOperatorDefinition(SyntaxTags.Ldelem) },
                    { "ldelem.ref", new TrivialOperatorDefinition(SyntaxTags.Ldelem) },

                    { "stelem.i1", new TrivialOperatorDefinition(SyntaxTags.Stelem) },
                    { "stelem.i2", new TrivialOperatorDefinition(SyntaxTags.Stelem) },
                    { "stelem.i4", new TrivialOperatorDefinition(SyntaxTags.Stelem) },
                    { "stelem.i8", new TrivialOperatorDefinition(SyntaxTags.Stelem) },
                    { "stelem.r4", new TrivialOperatorDefinition(SyntaxTags.Stelem) },
                    { "stelem.r8", new TrivialOperatorDefinition(SyntaxTags.Stelem) },
                    { "stelem.i", new TrivialOperatorDefinition(SyntaxTags.Stelem) },
                    { "stelem.ref", new TrivialOperatorDefinition(SyntaxTags.Stelem) },

                    { "ldc.i4.0", new LdcOperatorDefinition() },
                    { "ldc.i4.1", new LdcOperatorDefinition() },
                    { "ldc.i4.2", new LdcOperatorDefinition() },
                    { "ldc.i4.3", new LdcOperatorDefinition() },
                    { "ldc.i4.4", new LdcOperatorDefinition() },
                    { "ldc.i4.5", new LdcOperatorDefinition() },
                    { "ldc.i4.6", new LdcOperatorDefinition() },
                    { "ldc.i4.7", new LdcOperatorDefinition() },
                    { "ldc.i4.8", new LdcOperatorDefinition() },
                    { "ldc.i4.m1", new LdcOperatorDefinition() },
                    { "ldc.i4.M1", new LdcOperatorDefinition() },

                    { "brtrue", new LabelOperatorDefinition(SyntaxTags.Brtrue) },
                    { "brtrue.s", new LabelOperatorDefinition(SyntaxTags.Brtrue) },
                    { "brinst", new LabelOperatorDefinition(SyntaxTags.Brtrue) },
                    { "brinst.s", new LabelOperatorDefinition(SyntaxTags.Brtrue) },

                    { "brfalse", new LabelOperatorDefinition(SyntaxTags.Brfalse) },
                    { "brfalse.s", new LabelOperatorDefinition(SyntaxTags.Brfalse) },
                    { "brnull", new LabelOperatorDefinition(SyntaxTags.Brfalse) },
                    { "brnull.s", new LabelOperatorDefinition(SyntaxTags.Brfalse) },
                    { "brzero", new LabelOperatorDefinition(SyntaxTags.Brfalse) },
                    { "brzero.s", new LabelOperatorDefinition(SyntaxTags.Brfalse) },

                    { "stloc", new NumOperatorDefinition(SyntaxTags.Stloc) },
                    { "stloc.s", new NumOperatorDefinition(SyntaxTags.Stloc) },
                    { "stloc.0", new NumOperatorDefinition(SyntaxTags.Stloc) },
                    { "stloc.1", new NumOperatorDefinition(SyntaxTags.Stloc) },
                    { "stloc.2", new NumOperatorDefinition(SyntaxTags.Stloc) },
                    { "stloc.3", new NumOperatorDefinition(SyntaxTags.Stloc) },

                    { "ldloc", new NumOperatorDefinition(SyntaxTags.Ldloc) },
                    { "ldloc.s", new NumOperatorDefinition(SyntaxTags.Ldloc) },
                    { "ldloc.0", new NumOperatorDefinition(SyntaxTags.Ldloc) },
                    { "ldloc.1", new NumOperatorDefinition(SyntaxTags.Ldloc) },
                    { "ldloc.2", new NumOperatorDefinition(SyntaxTags.Ldloc) },
                    { "ldloc.3", new NumOperatorDefinition(SyntaxTags.Ldloc) },

                    { "ldarg", new NumOperatorDefinition(SyntaxTags.Ldarg) },
                    { "ldarg.s", new NumOperatorDefinition(SyntaxTags.Ldarg) },
                    { "ldarg.0", new NumOperatorDefinition(SyntaxTags.Ldarg) },
                    { "ldarg.1", new NumOperatorDefinition(SyntaxTags.Ldarg) },
                    { "ldarg.2", new NumOperatorDefinition(SyntaxTags.Ldarg) },
                    { "ldarg.3", new NumOperatorDefinition(SyntaxTags.Ldarg) },
                },
            },
        };

        public IEnumerable<Declaration> DefaultDeclarations => new Declaration[]
        {
            new TypeInfo("int32"),
            new TypeInfo("float32"),
            new TypeInfo("bool"),
        };

        public IEnumerable<IPreprocessorCommand> PreprocessorCommands => new IPreprocessorCommand[]
        {
            
        };

        public IEnumerable<IPreprocessorDetector> PreprocessorDetectors => new IPreprocessorDetector[]
        {
            
        };
    }
}