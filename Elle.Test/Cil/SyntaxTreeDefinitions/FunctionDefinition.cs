﻿using System.Collections.Generic;
using Elle.Core.Data.Contexts;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.SyntaxTrees;
using Elle.Core.Data.Tokens;
using Elle.Core.Exceptions;
using Elle.Test.Cil.Enums;
using Elle.Test.Cil.OperatorDefinitions;
using Elle.Test.Cil.Scopes;
using Elle.Test.Cil.SyntaxTrees;
using Elle.Test.Cil.SyntaxTrees.Operators;
using Elle.Test.Cil.Tokens;
using Elle.Test.Utilities;

namespace Elle.Test.Cil.SyntaxTreeDefinitions
{
    public class FunctionDefinition : ISyntaxTreeDefinition
    {
        public Dictionary<string, IOperatorDefinition> Operators { get; } = new Dictionary<string, IOperatorDefinition>();

        public bool CanBeStart(SyntaxContext context) => context.Current.Tag == (int) Tags.Method;

        public SyntaxTree TryParse(SyntaxContext context)
        {
            context.ValidateSymbol("Invalid method signature", (int) Tags.Method);
            context.ValidateSymbol("Invalid method signature", (int) Tags.Public);
            context.ValidateSymbol("Invalid method signature", (int) Tags.Hidebysig);
            context.ValidateSymbol("Invalid method signature", (int) Tags.Static);
            context.ValidateSymbol("Invalid method signature", (int) Tags.Void);
            var name = (Word) context.ValidateSymbol("Invalid method signature", (int) Core.Data.Enums.Tags.Id);
            var fi = context.CurrentScope.Register<FunctionInfo>($"function {name.Value}", true);
            var function = new Function(fi);
            var paramId = 0;
            fi.Name = name;
            fi.IsDeclared = true;

            context.Scope(name, () =>
            {

                context.ValidateSymbol("Parameter list start expected", '(');
                while (context.Current != null && context.Current.Tag != ')')
                {
                    if (paramId > 0)
                        context.ValidateSymbol(", expected", ',');

                    var param = ParseParam(context, paramId++);
                    fi.Parameters.Add(param);
                }
                context.ValidateSymbol("Parameter list end expected", ')');

                context.ValidateSymbol("Invalid method signature", (int) Tags.Cil);
                context.ValidateSymbol("Invalid method signature", (int) Tags.Managed);

                context.ValidateSymbol("Method body expected", '{');
                context.ValidateSymbol("Stack size expected", (int) Tags.Maxstack);
                function.MaxStack = (Number) context.ValidateSymbol("Stack size expected", (int) Tags.Number);

                context.ValidateSymbol("locals init expected", (int)Tags.Locals);
                context.ValidateSymbol("locals init expected", (int)Tags.Init);
                context.ValidateSymbol("locals init: '(' expected", '(');

                var isFirstParam = true;
                while (context.Current != null && context.Current.Tag != ')')
                {
                    if (!isFirstParam)
                        context.ValidateSymbol(", expected", ',');

                    ParseLocal(context);
                    isFirstParam = false;
                }

                context.ValidateSymbol("locals init: ')' expected", ')');
                var labelTable = new Dictionary<string, Label>();

                while (context.Current != null && context.Current.Tag != '}')
                {
                    var stmt = ParseStatement(context, labelTable);
                    function.Body.Add(stmt);
                }
            });
            context.ValidateSymbol("method body end expected", '}');

            return function;
        }

        private static VariableInfo ParseParam(SyntaxContext context, int id)
        {
            var vi = context.CurrentScope.Register<VariableInfo>($"param {id}", true);
            var typeName = (Word) context.ValidateSymbol("Parameter type expected", (int)Core.Data.Enums.Tags.Id);
            vi.Name = id.ToString();
            vi.Type = context.CurrentScope.FindDeclaration<TypeInfo>(typeName, "type");
            vi.IsArray = context.Current.Tag == '[';
            vi.IsDeclared = true;

            if (vi.IsArray)
            {
                context.Next();
                context.ValidateSymbol("] expected", ']');
            }

            var name = (Word) context.ValidateSymbol("Parameter name expected", (int)Core.Data.Enums.Tags.Id);

            return vi;
        }

        private static VariableInfo ParseLocal(SyntaxContext context)
        {
            context.ValidateSymbol("[ expected", '[');
            var id = (Number) context.ValidateSymbol("Local id expected", (int) Tags.Number);
            var vi = context.CurrentScope.Register<VariableInfo>($"local {id.Value}", true);
            context.ValidateSymbol("] expected", ']');
            var typeName = (Word)context.ValidateSymbol("Parameter type expected", (int)Core.Data.Enums.Tags.Id);
            vi.Name = id.ToString();
            vi.Type = context.CurrentScope.FindDeclaration<TypeInfo>(typeName, "type");
            vi.IsDeclared = true;
            var name = (Word)context.ValidateSymbol("Parameter name expected", (int)Core.Data.Enums.Tags.Id);

            return vi;
        }

        private Operator ParseStatement(SyntaxContext context, Dictionary<string, Label> labelTable)
        {
            var label = (Word) context.ValidateSymbol("Label name expected", (int)Core.Data.Enums.Tags.Id);
            context.ValidateSymbol(": expected", ':');

            var op = (Word) context.ValidateSymbol("Operator expected", (int)Core.Data.Enums.Tags.Id);
            
            IOperatorDefinition opDef;
            if (!Operators.TryGetValue(op, out opDef))
                throw new ParseException("Unknown operator");
            
            var parsed = opDef.TryParse(context, op, labelTable);
            parsed.Label = labelTable.FindOrCreate(label);

            return parsed;
        }
    }
}