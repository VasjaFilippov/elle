﻿using Elle.Core.Data.Tokens;
using Elle.Test.Cil.Enums;

namespace Elle.Test.Cil.Tokens
{
    public class Number : Token<int>
    {
        public Number(int value) : base((int)Tags.Number, value)
        {
        }
    }
}