﻿namespace Elle.Test.Cil.SyntaxTrees.Operators
{
    public class LabelOperator : Operator
    {
        public Label Value { get; }
        public override int Tag { get; }

        public LabelOperator(int tag, Label value)
        {
            Value = value;
            Tag = tag;
        }
    }
}