﻿using System.Collections.Generic;
using Elle.Test.Cil.Enums;

namespace Elle.Test.Cil.SyntaxTrees.Operators
{
    public class CallOperator : Operator
    {
        public override int Tag { get; } = (int) SyntaxTags.Call;

        public string ReturnType { get; set; }
        public string Name { get; set; }
        public List<string> ParameterTypes { get; } = new List<string>();
    }
}