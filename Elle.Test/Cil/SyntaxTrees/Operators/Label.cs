﻿namespace Elle.Test.Cil.SyntaxTrees.Operators
{
    public class Label
    {
        public string Name { get; }
        public int Usages { get; set; }

        public Label(string name)
        {
            Name = name;
        }
    }
}