﻿namespace Elle.Test.Cil.SyntaxTrees.Operators
{
    public class NumOperator : Operator
    {
        public int Value { get; }
        public override int Tag { get; }

        public NumOperator(int tag, int value)
        {
            Value = value;
            Tag = tag;
        }
    }
}