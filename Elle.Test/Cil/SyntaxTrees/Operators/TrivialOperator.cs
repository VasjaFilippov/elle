﻿using Elle.Test.Cil.Enums;

namespace Elle.Test.Cil.SyntaxTrees.Operators
{
    public class TrivialOperator : Operator
    {
        public override int Tag { get; }

        public TrivialOperator(int tag)
        {
            Tag = tag;
        }
    }
}