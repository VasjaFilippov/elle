﻿using Elle.Core.Data.SyntaxTrees;
using Elle.Test.Cil.SyntaxTrees.Operators;

namespace Elle.Test.Cil.SyntaxTrees
{
    public abstract class Operator : SyntaxTree
    {
        public Label Label { get; set; }
    }
}