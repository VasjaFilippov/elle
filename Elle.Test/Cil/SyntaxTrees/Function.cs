﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Elle.Core.Data.SyntaxTrees;
using Elle.Test.Cil.Enums;
using Elle.Test.Cil.Scopes;

namespace Elle.Test.Cil.SyntaxTrees
{
    [DebuggerDisplay("{DebugView}")]
    public class Function : SyntaxTree
    {
        public FunctionInfo Info { get; set; }
        public string Name => Info.Name;
        public TypeInfo ReturnType => Info.ReturnType;
        public List<SyntaxTree> Body { get; } = new List<SyntaxTree>();
        public List<VariableInfo> Parameters => Info.Parameters;
        public int MaxStack { get; set; }
        
        public string TypeParameterList => "void";

        public string ParameterList
            => Parameters.Any()
                ? string.Join(", ", Parameters.Select(p => $"{p.Type.Name} {p.Name}"))
                : "void";

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string ReturnTypeOrNothing
            => ReturnType?.Name ?? "void";

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string DebugView => $"{ReturnTypeOrNothing} {Name}<{TypeParameterList}>({ParameterList})";

        public override int Tag => (int) SyntaxTags.Function;

        public Function()
        {
            
        }

        public Function(FunctionInfo info)
        {
            Info = info;
        }
    }
}