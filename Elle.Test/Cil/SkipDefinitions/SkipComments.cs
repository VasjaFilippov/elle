﻿using Elle.Core.Data.Contexts;
using Elle.Core.Data.Intrefaces;

namespace Elle.Test.Cil.SkipDefinitions
{
    public class SkipComments : ISkipDefinition
    {
        public bool TrySkip(LexerContext context) => context.Current == '/' && TrySkipComment(context);
        
        private static bool TrySkipComment(LexerContext context)
        {
            var cur = context.Pop();
            var secondChar = context.Pop();

            switch (secondChar)
            {
                case '/':
                    SkipSingleLineComment(context);
                    return true;
                case '*':
                    SkipMultiLineComment(context);
                    return true;
                default:
                    context.Push(secondChar);
                    context.Push(cur);
                    return false;
            }
        }

        private static void SkipSingleLineComment(LexerContext context)
        {
            while (context.Pop() != '\n');
        }

        private static void SkipMultiLineComment(LexerContext context)
        {
            var secondChar = context.Pop();

            while (true)
            {
                var cur = secondChar;
                secondChar = context.Pop();

                if (cur == '*' && secondChar == '/')
                    break;
            }
        }
    }
}