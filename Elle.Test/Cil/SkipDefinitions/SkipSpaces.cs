﻿using Elle.Core.Data.Contexts;
using Elle.Core.Data.Intrefaces;

namespace Elle.Test.Cil.SkipDefinitions
{
    public class SkipSpaces : ISkipDefinition
    {
        public bool TrySkip(LexerContext context)
        {
            var c = context.Current;
            if (c != '\n')
            {
                if (c != ' ' && c != '\t' && c != '\r')
                    return false;

                context.Next();
                return true;
            }
            
            context.Next();
            context.NextLine();
            return true;
        }
    }
}