﻿using System.Collections.Generic;
using Elle.Core.Data.Scopes;

namespace Elle.Test.Cil.Scopes
{
    public class FunctionInfo : Declaration
    {
        public List<VariableInfo> Parameters { get; } = new List<VariableInfo>();
        public TypeInfo ReturnType { get; set; }
        public bool IsNative { get; set; }
        public bool IsConstant { get; set; }
    }
}