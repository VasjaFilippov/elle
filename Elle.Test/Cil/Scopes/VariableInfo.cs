﻿using Elle.Core.Data.Scopes;

namespace Elle.Test.Cil.Scopes
{
    public class VariableInfo : Declaration
    {
        public bool IsConstant { get; set; }
        public bool IsArray { get; set; }
        public TypeInfo Type { get; set; }
    }
}