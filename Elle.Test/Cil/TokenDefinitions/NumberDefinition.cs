﻿using Elle.Core.Data.Contexts;
using Elle.Core.Data.TokenDefinitions;
using Elle.Core.Data.Tokens;
using Elle.Test.Cil.Tokens;

namespace Elle.Test.Cil.TokenDefinitions
{
    public class NumberDefinition : ITokenDefinition
    {
        //private static readonly FloatDefinition FloatDefinition = new FloatDefinition();

        public bool CanBeStart(LexerContext context) => char.IsDigit(context.Current);

        public Token TryParse(LexerContext context)
        {
            var v = 0;

            do
            {
                v = v * 10 + (context.Pop() - '0');

                //if (context.Next() == '.')
                //    return FloatDefinition.TryParse(v, context);
            }
            while (char.IsDigit(context.Current));

            return new Number(v);
        }
    }
}