﻿using System.Collections.Generic;
using Elle.Test.Cil.SyntaxTrees.Operators;

namespace Elle.Test.Utilities
{
    public static class LabelDictionatyExtensions
    {
        public static Label FindOrCreate(this Dictionary<string, Label> labelTable, string name)
        {
            Label lbl;
            if (labelTable.TryGetValue(name, out lbl))
                return lbl;

            lbl = new Label(name);
            labelTable.Add(name, lbl);
            return lbl;
        }
    }
}