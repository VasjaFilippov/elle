﻿using System;
using Elle.Core.Data.Souces;

namespace Elle.Test
{
    public class ConsoleCodeSource : ISymbolSource<char>
    {
        public char Next()
            => Console.ReadKey().KeyChar;

        public bool HasNext()
            => true;
    }
}