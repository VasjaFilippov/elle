﻿using System;
using System.Collections.Generic;
using System.Linq;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.Scopes;
using Elle.Core.Data.TokenDefinitions;
using Elle.Core.Data.Tokens;

namespace Elle.Core.Utilities
{
    public class AssembledLanguage : ILanguage
    {
        private readonly ILanguage[] _languages;

        public AssembledLanguage(params ILanguage[] languages)
        {
            _languages = languages;
        }
        
        public IEnumerable<ISkipDefinition> SkipDefinitions => Assemble(l => l.SkipDefinitions);
        public IEnumerable<ITokenDefinition> Tokens => Assemble(l => l.Tokens);
        public IEnumerable<Word> ReservedWords => Assemble(l => l.ReservedWords);
        public IEnumerable<ISyntaxTreeDefinition> SyntaxTrees => Assemble(l => l.SyntaxTrees);
        public IEnumerable<Declaration> DefaultDeclarations => Assemble(l => l.DefaultDeclarations);
        public IEnumerable<IPreprocessorCommand> PreprocessorCommands => Assemble(l => l.PreprocessorCommands);
        public IEnumerable<IPreprocessorDetector> PreprocessorDetectors => Assemble(l => l.PreprocessorDetectors);
        public IDictionary<int, ISyntaxTranslator> Translators => new Dictionary<int, ISyntaxTranslator>();

        private IEnumerable<TElement> Assemble<TElement>(Func<ILanguage, IEnumerable<TElement>> property)
            => _languages
                .Select(property)
                .SelectMany(e => e);
    }
}