﻿using System;
using System.Collections.Generic;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.Souces;
using Elle.Core.Data.SyntaxTrees;

namespace Elle.Core.Logics
{
    public class CodeGenerator : ISymbolSource<string>
    {
        private ISymbolSource<SyntaxTree> _syntaxTreeSource;
        private IDictionary<int, ISyntaxTranslator> _translators;

        public void Prepare(ILanguage lang, ISymbolSource<SyntaxTree> syntaxTreeSource)
        {
            _translators = lang.Translators;
            _syntaxTreeSource = syntaxTreeSource;
        }

        public string GenerateConstruction()
        {
            var tree = _syntaxTreeSource.Next();

            if (tree == null)
                return null;

            ISyntaxTranslator translator;
            if (!_translators.TryGetValue(tree.Tag, out translator))
                throw new Exception("Translator not found");

            return translator.Translate(tree);
        }

        private string _buffer;

        public string Next()
        {
            if (_buffer == null)
                return GenerateConstruction();

            var tmp = _buffer;
            _buffer = null;
            return tmp;
        }

        public bool HasNext()
        {
            if (_buffer != null)
                return true;

            _buffer = GenerateConstruction();
            return _buffer != null;
        }
    }
}