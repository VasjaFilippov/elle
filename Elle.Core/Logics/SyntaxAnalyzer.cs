﻿#if !DEBUG
    #define CATCH_ERRORS
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using Elle.Core.Data.Contexts;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.Scopes;
using Elle.Core.Data.Souces;
using Elle.Core.Data.SymbolSources;
using Elle.Core.Data.SyntaxTrees;
using Elle.Core.Data.Tokens;

namespace Elle.Core.Logics
{
    public class SyntaxAnalyzer : ISymbolSource<SyntaxTree>
    {
        public SyntaxContext Context { get; private set; }

        private StreamingSymbolBuffer<Token> _tokensBuffer;
        private IEnumerable<ISyntaxTreeDefinition> _definitions;

        public void SetInput(ISymbolSource<Token> tokensSource)
        {
            _tokensBuffer = new StreamingSymbolBuffer<Token>(tokensSource);
        }

        public void Prepare(ILanguage lang)
        {
            _definitions = lang.SyntaxTrees;

            Context = new SyntaxContext(_tokensBuffer);
            foreach (var declaration in lang.DefaultDeclarations)
            {
                declaration.IsDeclared = true;
                Context.RootScope.TryRegister(declaration.ScopeName, declaration);
            }
        }

        public void SetDependancies(Scope dependancies)
        {
            Context.RootScope.Dependancies = dependancies;
        }

        public List<SyntaxTree> Scan()
        {
            var buffer = new List<SyntaxTree>();

            while (HasNext())
                buffer.Add(Next());

            return buffer;
        }

        public Scope ScanForDeclarations()
        {
            while (HasNext())
                Next();

            return Context.RootScope;
        }

        public SyntaxTree SubScan(int endBlockTag = -1)
        {
            while (true)
            {
                if (Context.RestTrees.Any())
                {
                    var tmp = Context.RestTrees.First();
                    Context.RestTrees.Remove(tmp);
                    return tmp;
                }

                var token = Context.Current;
                if (token == null || token.Tag == endBlockTag)
                    return null;

                _tokensBuffer.SetCheckpoint();
                Context.SkipParsedTree = false;
                Exception exception = null;

                foreach (var definition in _definitions)
                    if (definition.CanBeStart(Context))
                    {
                        SyntaxTree tree = null;
#if CATCH_ERRORS
                        try
                        {
#endif
                            tree = definition.TryParse(Context);
#if CATCH_ERRORS
                        }
                        catch (Exception e)
                        {
                            exception = e;
                        }
#endif

                        if (tree == null)
                        {
                            _tokensBuffer.GotoCheckpoint();
                            continue;
                        }

                        if (Context.SkipParsedTree)
                            break;
                        return tree;
                    }
                
                if (!Context.SkipParsedTree)
                    throw exception ?? new Exception("Unexpected construction");
            }
        }

        private SyntaxTree _buffer;

        public SyntaxTree Next()
        {
            if (_buffer == null)
                return SubScan();

            var tmp = _buffer;
            _buffer = null;
            return tmp;
        }

        public bool HasNext()
        {
            if (_buffer != null)
                return true;

            _buffer = SubScan();
            return _buffer != null;
        }
    }
}