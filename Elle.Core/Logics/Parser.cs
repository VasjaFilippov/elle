﻿using System;
using Elle.Core.Exceptions;

namespace Elle.Core.Logics
{
    public class Parser
    {
        private char _next;

        private static char GetChar() => Console.ReadKey().KeyChar;

        public Parser()
        {
            _next = GetChar();
        }

        public void Expression()
        {
            var continueExpr = true;
            Term();

            while (continueExpr)
            {
                switch (_next)
                {
                    case '+':
                        Match('+');
                        Term();
                        Console.Write('+');
                        break;
                    case '-':
                        Match('-');
                        Term();
                        Console.Write('-');
                        break;
                    default:
                        continueExpr = false;
                        break;
                }
            }
        }

        private void Term()
        {
            if (!char.IsDigit(_next))
                throw new ParseException("Syntax error");

            Console.Write(_next);
            Match(_next);
        }

        private void Match(char ch)
        {
            if (_next == ch)
                _next = GetChar();
            else
                throw new ParseException("Syntax error");
        }
    }
}
