﻿using System;
using System.Collections.Generic;
using Elle.Core.Data.Contexts;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.Souces;
using Elle.Core.Data.SymbolSources;
using Elle.Core.Data.TokenDefinitions;
using Elle.Core.Data.Tokens;

namespace Elle.Core.Logics
{
    public class Lexer : ISymbolSource<Token>
    {
        private readonly TokenDefinition _unknownDefinition = new TokenDefinition();
        private StreamingSymbolBuffer<char> _charsBuffer; 
        private LexerContext _context;
        private IEnumerable<ISkipDefinition> _skipDefinitions;
        private IEnumerable<ITokenDefinition> _definitions;

        public void SetInput(ISymbolSource<char> codeSource)
            => _charsBuffer = new StreamingSymbolBuffer<char>(codeSource);
        
        public void Prepare(ILanguage lang)
        {
            _context = new LexerContext(_charsBuffer);

            _skipDefinitions = lang.SkipDefinitions;
            _definitions = lang.Tokens;

            foreach (var reservedWord in lang.ReservedWords)
                _context.ReserveWord(reservedWord);
        }

        public Token Scan()
        {
            Exception exception = null;
            var continueSkipping = false;

            _charsBuffer.SetCheckpoint();
            do
            {
                foreach (var skipDefinition in _skipDefinitions)
                {
                    continueSkipping = skipDefinition.TrySkip(_context);
                    if (continueSkipping)
                    {
                        _charsBuffer.SetCheckpoint();
                        break;
                    }

                    _charsBuffer.GotoCheckpoint();
                }
            } while (continueSkipping);

            _charsBuffer.SetCheckpoint();
            foreach (var definition in _definitions)
            {
                if (!definition.CanBeStart(_context))
                    continue;

                try
                {
                    var temp = definition.TryParse(_context);
                    if (temp != null)
                        return temp;
                }
                catch (Exception e)
                {
                    exception = e;
                    _charsBuffer.GotoCheckpoint();
                }
            }

            if (exception != null)
                throw exception;

            return _unknownDefinition.TryParse(_context);
        }

        private Token _buffer;

        public Token Next()
        {
            if (_buffer == null)
                return Scan();

            var tmp = _buffer;
            _buffer = null;
            return tmp;
        }

        public bool HasNext()
        {
            if (_buffer != null)
                return true;

            _buffer = Scan();
            return _buffer != null;
        }
    }
}