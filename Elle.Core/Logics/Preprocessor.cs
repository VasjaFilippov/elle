﻿using System;
using System.Collections.Generic;
using System.Linq;
using Elle.Core.Data.Contexts;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.Souces;
using Elle.Core.Data.SymbolSources;
using Elle.Core.Data.Tokens;

namespace Elle.Core.Logics
{
    public class Preprocessor : ISymbolSource<Token>
    {
        public PreprocessorContext Context { get; private set; }
        public string Path { get; set; }
        public List<string> CustomPaths { get; } = new List<string>();

        private ISymbolSource<Token> _tokenSource;
        private IEnumerable<IPreprocessorCommand> _commands;
        private IEnumerable<IPreprocessorDetector> _detectors;

        public void SetInput(ISymbolSource<Token> tokenSource)
        {
            _tokenSource = tokenSource;
        }

        public void Prepare(ILanguage laguage)
        {
            var isss = new InsertedStackSymbolSource<Token>();
            isss.InsertSource(_tokenSource);

            Context = new PreprocessorContext(laguage, isss, Path) {CustomPaths = CustomPaths};
            _commands = laguage.PreprocessorCommands;
            _detectors = laguage.PreprocessorDetectors;
        }

        public Token Preprocess()
        {
            var token = Context.Pop();

            if (token == null)
                return null;

            if (!_detectors.Any(d => d.IsPreprocessorStart(token)))
                return token;
            
            foreach (var command in _commands)
            {
                if (!command.CanBeStart(Context))
                    continue;
                
                command.Preprocess(Context);
                return Preprocess();
            }

            throw new Exception("Unknown preprocessor command");
        }

        private Token _buffer;

        public Token Next()
        {
            if (_buffer == null)
                return Preprocess();

            var tmp = _buffer;
            _buffer = null;
            return tmp;
        }

        public bool HasNext()
        {
            if (_buffer != null)
                return true;

            _buffer = Preprocess();
            return _buffer != null;
        }
    }
}