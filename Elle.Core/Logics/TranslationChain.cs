﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.Scopes;
using Elle.Core.Data.Souces;
using Elle.Core.Data.SymbolSources;
using Elle.Core.Data.SyntaxTrees;
using Elle.Core.Data.Tokens;

namespace Elle.Core.Logics
{
    public class TranslationChain : ISymbolSource<SyntaxTree>
    {
        private readonly Lexer _lexer;
        private readonly Preprocessor _preprocessor;
        private readonly SymbolCatchingBuffer<Token> _tokenCatcher;
        private readonly SyntaxAnalyzer _syntaxAnalyzer;
        private Scope _dependancies;
        private string _path;

        public IEnumerable<Declaration> Declarations => _syntaxAnalyzer.Context.RootScope.EnumerateRecurcively();

        public TranslationChain()
        {
            _lexer = new Lexer();
            _preprocessor = new Preprocessor();
            _tokenCatcher = new SymbolCatchingBuffer<Token>();
            _syntaxAnalyzer = new SyntaxAnalyzer();
            
            _preprocessor.SetInput(_lexer);
            _tokenCatcher.SetInput(_preprocessor);
            _syntaxAnalyzer.SetInput(_tokenCatcher);
        }

        public void SetTokenCatcher(Action<Token> catcher)
            => _tokenCatcher.SetWorker(catcher);

        public void Prepare(ILanguage language, ISymbolSource<char> codeSource)
        {
            SetInput(codeSource);
            SetUpChaint(language);
            _syntaxAnalyzer.SetDependancies(_dependancies);
        }

        public void Prepare(ILanguage language, string path)
        {
            SetInput(path);
            SetUpChaint(language);
            _syntaxAnalyzer.SetDependancies(_dependancies);
        }

        public void ParseDependancies(ILanguage language, ISymbolSource<char> codeSource)
        {
            SetInput(codeSource);
            SetUpChaint(language);
            _dependancies = _syntaxAnalyzer.ScanForDeclarations();
        }

        public void ParseDependancies(ILanguage language, string path)
        {
            SetInput(path);
            SetUpChaint(language);
            _dependancies = _syntaxAnalyzer.ScanForDeclarations();
        }

        public void ClearDependancies()
            => _dependancies = null;

        public SyntaxTree Next()
            => _syntaxAnalyzer.Next();

        public bool HasNext()
            => _syntaxAnalyzer.HasNext();

        private void SetInput(string path)
        {
            _path = path.Substring(0, path.LastIndexOf('\\'));
            var file = new FileCodeSource(path);
            _lexer.SetInput(file);
            _preprocessor.Path = _path;
        }

        private void SetInput(ISymbolSource<char> input)
        {
            _path = Assembly.GetExecutingAssembly().Location;
            _path = _path.Substring(0, _path.LastIndexOf('\\'));
            _lexer.SetInput(input);
        }

        private void SetUpChaint(ILanguage language)
        {
            _lexer.Prepare(language);
            _preprocessor.Prepare(language);
            _syntaxAnalyzer.Prepare(language);
        }
    }
}