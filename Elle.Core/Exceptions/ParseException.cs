﻿using System;

namespace Elle.Core.Exceptions
{
    public class ParseException : Exception
    {
        public ParseException(string msg) : base(msg)
        {

        }
    }
}