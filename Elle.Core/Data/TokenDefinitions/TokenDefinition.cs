﻿using Elle.Core.Data.Contexts;
using Elle.Core.Data.Tokens;

namespace Elle.Core.Data.TokenDefinitions
{
    public class TokenDefinition : ITokenDefinition
    {
        public bool CanBeStart(LexerContext context) => true;

        public Token TryParse(LexerContext context)
        {
            if (context.Current == 0)
                return null;

            var token = new Token(context.Pop());
            return token;
        }
    }
}