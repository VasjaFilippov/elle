﻿using Elle.Core.Data.Contexts;
using Elle.Core.Data.Tokens;

namespace Elle.Core.Data.TokenDefinitions
{
    public interface ITokenDefinition
    {
        bool CanBeStart(LexerContext context);
        Token TryParse(LexerContext context);
    }
}