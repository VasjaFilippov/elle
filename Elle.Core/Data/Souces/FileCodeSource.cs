﻿using System;
using System.IO;

namespace Elle.Core.Data.Souces
{
    public class FileCodeSource : ISymbolSource<char>, IDisposable
    {
        private StreamReader _file;
        private string _buffer = "";
        private int _bufferPosition;

        public FileCodeSource(string path)
        {
            _file = File.OpenText(path);
        }

        public void ReadToEnd()
        {
            _buffer += _file.ReadToEnd();
            _file.Close();
            _file = null;
        }

        public char Next()
            => HasNext()
                ? _buffer[_bufferPosition++]
                : default(char);

        public bool HasNext()
        {
            while (_bufferPosition >= _buffer.Length)
            {
                if (_file == null)
                    return false;

                if (_file.EndOfStream)
                {
                    _file.Close();
                    _file = null;
                    return false;
                }

                _buffer = $"{_file.ReadLine()}\r\n";
                _bufferPosition = 0;
            }

            return _bufferPosition < _buffer.Length;
        }

        public void Dispose()
        {
            if (_file == null)
                return;

            _file.Close();
            _file = null;
        }
    }
}