﻿namespace Elle.Core.Data.Souces
{
    public interface ISymbolSource<out TSymbol>
    {
        TSymbol Next();

        bool HasNext();
    }
}