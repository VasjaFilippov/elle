﻿namespace Elle.Core.Data.Souces
{
    public class DebugSymbolSource : ISymbolSource<char>
    {
        private readonly string _code;
        private int _position;

        public DebugSymbolSource(string code)
        {
            _code = code;
        }
        
        public char Next()
        {
            if (_position < _code.Length)
            {
                var c = _code[_position];
                _position++;
                return c;
            }

            return (char)0;
        }

        public bool HasNext()
            => _position < _code.Length;
    }
}