﻿namespace Elle.Core.Data.Tokens
{
    public class Word : Token<string>
    {
        public Word(int tag, string lexem) : base(tag, lexem)
        {
        }

        public static implicit operator string(Word word)
            => word?.Value;
    }
}