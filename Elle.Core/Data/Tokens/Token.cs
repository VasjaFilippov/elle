﻿namespace Elle.Core.Data.Tokens
{
    public class Token
    {
        public int Tag { get; }

        public Token(int tag)
        {
            Tag = tag;
        }
    }

    public class Token<T> : Token
    {
        public T Value { get; }

        public Token(int tag, T value) : base(tag)
        {
            Value = value;
        }

        public static implicit operator T(Token<T> token) => token.Value;
    }
}
