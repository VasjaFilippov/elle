﻿using Elle.Core.Data.Tokens;

namespace Elle.Core.Data.Intrefaces
{
    public interface IPreprocessorDetector
    {
        bool IsPreprocessorStart(Token token);
    }
}