﻿using System.Collections.Generic;
using Elle.Core.Data.Scopes;
using Elle.Core.Data.TokenDefinitions;
using Elle.Core.Data.Tokens;

namespace Elle.Core.Data.Intrefaces
{
    public interface ILanguage
    {
        IEnumerable<ISkipDefinition> SkipDefinitions { get; }
        IEnumerable<ITokenDefinition> Tokens { get; }
        IEnumerable<Word> ReservedWords { get; }
        IDictionary<int, ISyntaxTranslator> Translators { get; }
        IEnumerable<ISyntaxTreeDefinition> SyntaxTrees { get; }
        IEnumerable<Declaration> DefaultDeclarations { get; }
        IEnumerable<IPreprocessorCommand> PreprocessorCommands { get; }
        IEnumerable<IPreprocessorDetector> PreprocessorDetectors { get; }
    }
}