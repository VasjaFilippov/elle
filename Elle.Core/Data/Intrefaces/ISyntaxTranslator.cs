﻿using Elle.Core.Data.SyntaxTrees;

namespace Elle.Core.Data.Intrefaces
{
    public interface ISyntaxTranslator
    {
        string Translate(SyntaxTree tree);
    }
}