﻿using Elle.Core.Data.Contexts;

namespace Elle.Core.Data.Intrefaces
{
    public interface IPreprocessorCommand
    {
        bool CanBeStart(PreprocessorContext context);

        void Preprocess(PreprocessorContext context);
    }
}