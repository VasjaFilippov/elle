﻿using Elle.Core.Data.Contexts;
using Elle.Core.Data.SyntaxTrees;

namespace Elle.Core.Data.Intrefaces
{
    public interface ISyntaxTreeDefinition
    {
        bool CanBeStart(SyntaxContext context);
        SyntaxTree TryParse(SyntaxContext context);
    }
}