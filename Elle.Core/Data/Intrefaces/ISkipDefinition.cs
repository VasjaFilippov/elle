﻿using Elle.Core.Data.Contexts;

namespace Elle.Core.Data.Intrefaces
{
    public interface ISkipDefinition
    {
        bool TrySkip(LexerContext context);
    }
}