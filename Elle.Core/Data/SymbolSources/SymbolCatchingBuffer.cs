﻿using System;
using Elle.Core.Data.Souces;

namespace Elle.Core.Data.SymbolSources
{
    public class SymbolCatchingBuffer<TSymbol> : ISymbolSource<TSymbol>
    {
        private ISymbolSource<TSymbol> _source;
        private Action<TSymbol> _worker;

        public void SetInput(ISymbolSource<TSymbol> source)
            => _source = source;

        public void SetWorker(Action<TSymbol> worker)
            => _worker = worker;

        public TSymbol Next()
        {
            var symb = _source.Next();
            if (symb != null)
                _worker?.Invoke(symb);
            return symb;
        }

        public bool HasNext()
            => _source.HasNext();
    }
}