﻿using System.Collections.Generic;
using Elle.Core.Data.Souces;

namespace Elle.Core.Data.SymbolSources
{
    public class SymbolBuffer<TSymbol> : ISymbolSource<TSymbol>
    {
        public int Position { get; set; }

        public TSymbol[] Buffer { get; private set; }

        public void Fill(ISymbolSource<TSymbol> source)
        {
            var temp = new List<TSymbol>();

            while (source.HasNext())
                temp.Add(source.Next());

            Buffer = temp.ToArray();
            Position = 0;
        }

        public void Fill(params TSymbol[] symbols)
        {
            Buffer = symbols;
            Position = 0;
        }

        public TSymbol Next()
            => HasNext()
                ? Buffer[Position++]
                : default(TSymbol);

        public bool HasNext()
            => Position < Buffer.Length;
    }
}