﻿using System.Collections.Generic;
using Elle.Core.Data.Souces;

namespace Elle.Core.Data.SymbolSources
{
    public class StreamingSymbolBuffer<TSymbol> : ISymbolSource<TSymbol>
    {
        private readonly List<TSymbol> _buffer = new List<TSymbol>();
        private readonly ISymbolSource<TSymbol> _source;
        private int _positionInBuffer;

        public StreamingSymbolBuffer(ISymbolSource<TSymbol> source)
        {
            _source = source;
        }

        public TSymbol Next()
        {
            if (_positionInBuffer < _buffer.Count)
                return _buffer[_positionInBuffer++];

            var tmp = _source.Next();
            _buffer.Add(tmp);
            _positionInBuffer++;
            return tmp;
        }
        
        public bool HasNext()
            => _positionInBuffer < _buffer.Count || _source.HasNext();

        public void GotoCheckpoint()
            => _positionInBuffer = 0;

        public void SetCheckpoint()
        {
            _positionInBuffer = 0;
            _buffer.Clear();
        }
    }
}