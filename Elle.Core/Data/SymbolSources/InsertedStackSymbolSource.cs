﻿using System;
using System.Collections.Generic;
using System.Linq;
using Elle.Core.Data.Souces;

namespace Elle.Core.Data.SymbolSources
{
    public class InsertedStackSymbolSource<TSymbol> : IStackSymbolSource<TSymbol>
        where TSymbol : class
    {
        public event Action SourceEnded;

        public void InsertSource(ISymbolSource<TSymbol> source)
        {
            var isss = source as IStackSymbolSource<TSymbol>
                      ?? new StackSymbolSource<TSymbol>(source);
            _sources.Insert(0, isss);
        }

        public TSymbol Current => GetSource()?.Current;
        public TSymbol Next() => GetSource()?.Next();
        public bool HasNext() => GetSource()?.HasNext() ?? false;
        public TSymbol Pop() => GetSource()?.Pop();

        public void Push(TSymbol symbol)
        {
            var source = GetSource();
            if (source == null)
            {
                var buffer = new SymbolBuffer<TSymbol>();
                buffer.Fill(symbol);
                source = new StackSymbolSource<TSymbol>(buffer);
                _sources.Add(source);
            }
            else
                source.Push(symbol);
        }
        
        private readonly List<IStackSymbolSource<TSymbol>> _sources = new List<IStackSymbolSource<TSymbol>>();

        private IStackSymbolSource<TSymbol> GetSource()
        {
            while (true)
            {
                if (!_sources.Any())
                    return null;

                var source = _sources.First();
                if (source.Current != null)
                    return source;

                _sources.Remove(source);
                SourceEnded?.Invoke();
            }
        }
    }
}