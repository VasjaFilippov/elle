﻿using System.Collections.Generic;
using System.Linq;
using Elle.Core.Data.Souces;

namespace Elle.Core.Data.SymbolSources
{
    public class StackSymbolSource<TSymbol> : IStackSymbolSource<TSymbol>
    {
        public TSymbol Current { get; private set; }

        private readonly List<TSymbol> _nextSymbols = new List<TSymbol>();
        private readonly ISymbolSource<TSymbol> _stackSymbolsSource;

        public StackSymbolSource(ISymbolSource<TSymbol> source)
        {
            _stackSymbolsSource = source;

            var stackSymbolSource = source as IStackSymbolSource<TSymbol>;
            Current = stackSymbolSource != null
                ? stackSymbolSource.Current
                : _stackSymbolsSource.Next();
        }

        public bool HasNext()
            => _nextSymbols.Any() || _stackSymbolsSource.HasNext();

        public TSymbol Next()
        {
            if (!_nextSymbols.Any())
                return Current = _stackSymbolsSource.Next();

            var next = _nextSymbols[0];
            _nextSymbols.RemoveAt(0);
            return Current = next;
        }

        public TSymbol Pop()
        {
            var c = Current;
            Next();
            return c;
        }

        public void Push(TSymbol symbol)
        {
            _nextSymbols.Insert(0, Current);
            Current = symbol;
        }
    }
}