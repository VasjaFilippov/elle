﻿using Elle.Core.Data.Souces;

namespace Elle.Core.Data.SymbolSources
{
    public class ChainedSymbolSource<TSymbol> : ISymbolSource<TSymbol>
    {
        private ISymbolSource<TSymbol>[] _sources;
        private int _curSource;

        public void Fill(params ISymbolSource<TSymbol>[] sources)
            => _sources = sources;

        public TSymbol Next()
            => HasNext()
                ? _sources[_curSource].Next()
                : default(TSymbol);

        public bool HasNext()
        {
            while (_curSource < _sources.Length && !_sources[_curSource].HasNext())
                _curSource++;

            return _curSource < _sources.Length;
        }
    }
}