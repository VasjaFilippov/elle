﻿using Elle.Core.Data.Souces;

namespace Elle.Core.Data.SymbolSources
{
    public interface IStackSymbolSource<TSymbol> : ISymbolSource<TSymbol>
    {
        TSymbol Current { get; }

        TSymbol Pop();
        void Push(TSymbol symbol);
    }
}