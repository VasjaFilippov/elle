﻿namespace Elle.Core.Data.SyntaxTrees
{
    public abstract class SyntaxTree
    {
        public abstract int Tag { get; }
    }
}