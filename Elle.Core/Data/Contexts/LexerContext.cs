﻿using System.Collections.Generic;
using Elle.Core.Data.Souces;
using Elle.Core.Data.SymbolSources;
using Elle.Core.Data.Tokens;

namespace Elle.Core.Data.Contexts
{
    public class LexerContext : StackSymbolSource<char>
    {
        public Dictionary<string, Word> Words { get; } = new Dictionary<string, Word>();
        public long Line { get; private set; }

        public LexerContext(ISymbolSource<char> codeSource) : base(codeSource)
        {
        }

        public void ReserveWord(Word word) => Words[word.Value] = word;
        public void NextLine() => Line++;
    }
}