using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Elle.Core.Data.Intrefaces;
using Elle.Core.Data.Souces;
using Elle.Core.Data.SymbolSources;
using Elle.Core.Data.Tokens;
using Elle.Core.Logics;

namespace Elle.Core.Data.Contexts
{
    public class PreprocessorContext : IStackSymbolSource<Token>
    {
        public ILanguage Language { get; }
        public string CurPath { get; private set; }
        public List<string> CustomPaths { get; internal set; }

        public PreprocessorContext(ILanguage lang, InsertedStackSymbolSource<Token> stackSymbolsSource, string path)
        {
            _source = stackSymbolsSource;
            Language = lang;
            CurPath = path;

            _source.SourceEnded += SourceEnded;
        }

        public void InsertSource(ISymbolSource<Token> source)
        {
            _paths.Push(CurPath);
            _source.InsertSource(source);
        }

        public void InsertSource(string path)
        {
            var pathFound = false;

            if (CurPath != null)
            {
                var tmpPath = $@"{CurPath}\{path}";
                if (File.Exists(tmpPath))
                {
                    path = tmpPath;
                    pathFound = true;
                }
            }

            if (!pathFound)
            {
                path = CustomPaths
                    .Select(customPath => $@"{customPath}\{path}")
                    .FirstOrDefault(File.Exists) ?? path;
            }

            var file = new FileCodeSource(path);
            var lexer = new Lexer();
            
            lexer.SetInput(file);
            lexer.Prepare(Language);
            InsertSource(lexer);
            CurPath = path;
        }

        public Token ValidateSymbol(string errorMsg, int tag)
        {
            var token = Pop();
            if (token.Tag != tag)
                throw new Exception(errorMsg);
            return token;
        }

        public Token ValidateSymbol(string errorMsg, params int[] tags)
        {
            var token = Pop();
            if (tags.All(tag => token.Tag != tag))
                throw new Exception(errorMsg);
            return token;
        }
        
        public Token Current => _source.Current;
        public Token Next() => _source.Next();
        public bool HasNext() => _source.HasNext();
        public Token Pop() => _source.Pop();
        public void Push(Token symbol) => _source.Push(symbol);

        private readonly InsertedStackSymbolSource<Token> _source;
        private readonly Stack<string> _paths = new Stack<string>();
        
        private void SourceEnded()
        {
            CurPath = _paths.Any()
                ? _paths.Pop()
                : null;
        }
    }
}