﻿using System;
using System.Collections.Generic;
using System.Linq;
using Elle.Core.Data.Scopes;
using Elle.Core.Data.Souces;
using Elle.Core.Data.SymbolSources;
using Elle.Core.Data.SyntaxTrees;
using Elle.Core.Data.Tokens;

namespace Elle.Core.Data.Contexts
{
    public class SyntaxContext : StackSymbolSource<Token>
    {
        public Scope RootScope { get; }
        public Scope CurrentScope { get; private set; }
        public object State { get; set; }
        public List<SyntaxTree> RestTrees { get; } = new List<SyntaxTree>();
        public bool SkipParsedTree { get; set; }

        public SyntaxContext(ISymbolSource<Token> symbolsSource) : base(symbolsSource)
        {
            RootScope = new Scope();
            CurrentScope = RootScope;
        }

        public Token ValidateSymbol(string errorMsg, int tag)
        {
            var token = Pop();
            if (token?.Tag != tag)
                throw new Exception(errorMsg);
            return token;
        }

        public Token ValidateSymbol(string errorMsg, params int[] tags)
        {
            var token = Pop();
            if (tags.All(tag => token.Tag != tag))
                throw new Exception(errorMsg);
            return token;
        }

        public void Scope(string name, Action content)
        {
            CurrentScope = new Scope(name, CurrentScope);
            content();
            CurrentScope = CurrentScope.Parent;
        }

        public void Scope(Func<string> content)
        {
            var prevScope = CurrentScope;
            CurrentScope = new Scope(prevScope);
            var scopeName = content();
            CurrentScope.SetParent(scopeName, prevScope);
            CurrentScope = prevScope;
        }

        public void GlobalScope(Action content)
        {
            var tempCope = CurrentScope;
            CurrentScope = RootScope;
            content();
            CurrentScope = tempCope;
        }
    }
}