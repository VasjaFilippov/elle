﻿namespace Elle.Core.Data.Scopes
{
    public abstract class Declaration
    {
        public string Name { get; set; }
        public string ScopeName { get; set; } 
        public bool IsDeclared { get; set; }
        public string Path { get; set; }

        public string FullPath => $"{Path}.{ScopeName}";
    }
}