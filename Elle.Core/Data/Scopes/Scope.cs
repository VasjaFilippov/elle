﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Elle.Core.Data.Scopes
{
    public class Scope
    {
        public Scope Dependancies { get; set; }
        public Dictionary<string, Scope> NestedScopes { get; } = new Dictionary<string, Scope>();
        public Scope Parent { get; private set; }
        public string Name { get; private set; }
        public string FullPath => $"{Parent?.FullPathWithDot ?? string.Empty}{Name}";
        public string FullPathWithDot => $"{FullPath}.";

        private readonly Dictionary<string, Declaration> _table = new Dictionary<string, Declaration>();

#if PRINT_DEBUG
        private static int _prevCount;
        private static int _count;
        private static bool _labelPrinted;
#endif

        public Scope(Scope parent = null)
        {
            Name = "global";
            Parent = parent;
#if PRINT_DEBUG
            if (!_labelPrinted)
            {
                Console.SetCursorPosition(0, 0);
                Console.Write("Objects declared: 0");
                _labelPrinted = true;
            }
#endif
        }

        public Scope(string name, Scope parent)
        {
            if (name.Contains(' ') && name[0] != '{')
                name = $"{{{name}}}";

            SetParent(name, parent);
        }

        public TDeclaration FindDeclaration<TDeclaration>(string name, string prefix)
            where TDeclaration : Declaration, new()
        {
            if (prefix != null && !name.StartsWith(prefix))
                name = $"{prefix} {name}";
            if (name.Contains(' ') && name[0] != '{')
                name = $"{{{name}}}";

            var type = this[name];
            if (type == null)
            {
                var newType = Register<TDeclaration>(name, false);
                newType.IsDeclared = false;
                return newType;
            }

            if (!(type is TDeclaration))
                throw new Exception($"Declared object with name <{name}> in scope <{FullPath}> is not a {prefix}");
            return (TDeclaration)type;
        }

        public void SetParent(string name, Scope parent)
        {
            if (name.Contains(' ') && name[0] != '{')
                name = $"{{{name}}}";

            Name = name;
            Parent = parent;

            if (parent == null) return;

            parent.NestedScopes.Add(name, this);
            UpdatePath();
        }

        private void UpdatePath()
        {
            foreach (var decl in _table)
                decl.Value.Path = FullPath;
            foreach (var scope in NestedScopes)
                scope.Value.UpdatePath();
        }

        public TDeclaration Register<TDeclaration>(string name, bool isDeclaration)
            where TDeclaration : Declaration, new()
        {
            TDeclaration value;
            Declaration dValue;

            if (name.Contains(' ') && name[0] != '{')
                name = $"{{{name}}}";

            if ((dValue = Dependancies?[name]) != null || _table.TryGetValue(name, out dValue))
            {
                value = dValue as TDeclaration;
                if (value == null || isDeclaration && dValue.IsDeclared)
                    throw new Exception($"Scope member redeclared: {name}");
            }
            else
                value = new TDeclaration();

            value.ScopeName = name;
            value.Path = FullPath;
            _table[name] = value;

#if PRINT_DEBUG
            if (++_count - _prevCount > 100)
            {
                _prevCount = _count;
                Console.SetCursorPosition(18, 0);
                Console.Write(_count);
            }
#endif
            return value;
        }

        public void TryRegister(string name, Declaration value)
        {
            Declaration dValue;

            if (name.Contains(' ') && name[0] != '{')
                name = $"{{{name}}}";
            
            if (Dependancies?[name] != null || _table.TryGetValue(name, out dValue))
                throw new Exception($"Scope member redeclared: {name}");

            value.ScopeName = name;
            value.Path = FullPath;
            _table[name] = value;

#if PRINT_DEBUG
            if (++_count - _prevCount > 100)
            {
                _prevCount = _count;
                Console.SetCursorPosition(18, 0);
                Console.Write(_count);
            }
#endif
        }

        public IEnumerable<Declaration> EnumerateRecurcively()
        {
            return NestedScopes
                .SelectMany(sKvp => sKvp.Value.EnumerateRecurcively())
                .Concat(_table.Values);
        }

        public Declaration this[string key]
        {
            get
            {
                if (key.Contains(' ') && key[0] != '{')
                    key = $"{{{key}}}";

                Declaration temp;
                return _table.TryGetValue(key, out temp)
                    ? temp
                    : Parent?[key] ?? Dependancies?[key];
            }
        }
    }
}